set -e
echo "linting all in predictions/. Showing errors only:"
pylint -E predictions
echo "Running unit tests"
py.test
