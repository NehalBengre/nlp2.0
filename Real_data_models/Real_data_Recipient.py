import bson
import json

with open('test/actionable_semantics.bson', 'rb') as f:
    data = bson.decode_all(f.read())

with open('test/user_query.bson', 'rb') as f:
    data2 = bson.decode_all(f.read())

with open('test/qu_state.bson', 'rb') as f:
    data3 = bson.decode_all(f.read())


def labels(i, sentence):
    contact_names = []
    for values in data3[i]['slots']:
        if values['origin_type'] == 'CONTACT_NAME':
            name = values['origin_value']

            if name not in sentence:
                if len(name.split(" ")) == 2:

                    ind = sentence.index(name.split(" ")[0])
                    contact_names.append(sentence[ind] + " " + sentence[ind + 1])
                elif "call" in sentence:
                    ind = sentence.index("call")
                    contact_names.append(sentence[ind + 1])
                elif "talk" in sentence:
                    ind = sentence.index("to")
                    contact_names.append(sentence[ind + 1])
                elif "contact" in sentence:
                    ind = sentence.index("contact")
                    # print(ind,sentence[ind+1])
                    contact_names.append(sentence[ind + 1])
            else:

                ind = sentence.index(name)

                if len(name.split(" ")) > 1:

                    contact_names.append(sentence[ind] + " " + sentence[ind + 1])
                else:

                    contact_names.append(sentence[ind])
                    # contact_names.append(value['origin_value'])

    return list(set(contact_names))


import nltk

list_sentences = {}
for i, values in enumerate(data):
    if values['user_intent'] == 'MAKE_CALL' and values['machine_action'] == 'confirm':
        sentence = data2[i]['utterances'][0]['utterance'].lower()
        tokenized_sentence = nltk.word_tokenize(sentence)
        if "call" in sentence or "contact" in sentence or "ring" in sentence or "talk" in sentence:
            if "Tell" in sentence or "tell" in sentence:
                break
            else:
                if data2[i]['utterances'][0]['utterance'] not in list_sentences:
                    list_sentences[data2[i]['utterances'][0]['utterance']] = labels(i, tokenized_sentence)
                    # labels(data2[i]['utterances'][0]['utterance'])
                    # print(labels(i,tokenized_sentence))

list_sentences["call bi-weekly"] = ['bi-weekly']
list_sentences["call Nehal Bengre"] = ['Nehal Bengre']
list_sentences["call Jennifer"] = ['Jennifer']
list_sentences["call Siwei"] = ['Siwei']
list_sentences["call Cisco"] = ['Cisco']
list_sentences["call Smurf"] = ['Smurf']
list_sentences["call Drunken Chicken"] = ['Drunken Chicken']
list_sentences["call Punit"] = ['Punit']
list_sentences["call Rolf"] = ['Rolf']
list_sentences["call Elliott"] = ['Elliott']
list_sentences["call Masaki"] = ['Masaki']
list_sentences["call Dean"] = ['Dean']
list_sentences["call Chris"] = ['Chris']
list_sentences["call Bob"] = ['Bob']

import nltk

make_call_tags = []

for values in list_sentences.keys():
    tags = [0] * 44
    tokenized = nltk.word_tokenize(values.lower())

    for v in list_sentences[values]:

        if len(v.lower().split(" ")) == 2:

            ind = tokenized.index(v.lower().split(" ")[0])
            tags[ind] = 1
            tags[ind + 1] = 1

        else:
            ind = tokenized.index(v.lower())
            tags[ind] = 1
    make_call_tags.append(tags)

send_confirm = []
indices = []
send_confirm_recipient = []
send_confirm_message = []

prev = ''
i = 0
j = 0
l = []
for values in data2:
    if 'add' in values['utterances'][0]['utterance'] or 'remove' in values['utterances'][0][
        'utterance'] or 'also send' in values['utterances'][0]['utterance'] or 'Also' in values['utterances'][0][
        'utterance'] or "Don't" in values['utterances'][0]['utterance'] or 'Add' in values['utterances'][0][
        'utterance']:
        # print(values)
        i = i + 1
        # print(values['utterances'][0]['utterance'])
        l.append(values['utterances'][0]['utterance'])
    j = j + 1
for i, values in enumerate(data):
    # print(i)
    if values['user_intent'] == "SEND_MESSAGE" and values['machine_action'] == "confirm" and prev != values['user_id']:
        prev = values['user_id']
        if data2[i]['utterances'][0]['utterance'] not in send_confirm and data2[i]['utterances'][0][
            'utterance'] not in l:
            if "tell" in data2[i]['utterances'][0]['utterance'] or "send" in data2[i]['utterances'][0][
                'utterance'] or "Send" in data2[i]['utterances'][0]['utterance'] or "let" in data2[i]['utterances'][0][
                'utterance'] or "Tell" in data2[i]['utterances'][0]['utterance']:
                if "as well" not in data2[i]['utterances'][0]['utterance']:

                    indices.append(i)
                    import ast

                    a = ast.literal_eval(values['data']['payload'])
                    recep = []
                    for v in data3[i]['slots']:

                        if v['type'] == 'CONTACT_NAME':
                            # print(i,a['message_value'],data2[i]['utterances'][0]['utterance'])
                            # print(i,data3.index(values1),"data3",data3[i],"data2",data2[i])
                            # print(v['raw_value'])

                            recep.append(v['raw_value'])

                        if a['message_value'] not in send_confirm_message:
                            send_confirm_recipient.append(recep)

                            send_confirm.append(data2[i]['utterances'][0]['utterance'])
                            send_confirm_message.append(a['message_value'])

send_message_confirm_tags = []
send_message_sentences = []
n = 0
for i, values in enumerate(send_confirm):
    tags = [0] * 44
    v = nltk.word_tokenize(values.lower())

    # rec = nltk.word_tokenize(send_confirm_recipient[i].lower())
    mes = nltk.word_tokenize(send_confirm_message[i].lower())

    for rec_values in send_confirm_recipient[i]:
        rec = nltk.word_tokenize(rec_values.lower())

        if len(rec) == 1:

            if rec[0] in v:
                ind_rec = v.index(rec[0])
                tags[ind_rec:ind_rec + len(rec)] = [1]
        else:
            if rec[0] in v:
                ind_rec = v.index(rec[0])

                tags[ind_rec:ind_rec + len(rec)] = [1] * len(rec)

    if mes[len(mes) - 1] in v:
        if mes[0] in v:
            ind_mes = v.index(mes[0])
            # print(ind_mes,len(mes))
            # print(tags)
            tags[ind_mes:ind_mes + len(mes)] = [2] * (len(mes))
            if (len(tags)) > 44:
                tags = tags[:44]

            if 1 in tags:
                send_message_confirm_tags.append(tags)
                n = n + 1

                send_message_sentences.append(send_confirm[i])

final_send_message_confirm_tags = []
final_send_message_sentences = []
for i in range(len(send_message_confirm_tags)):
    if len(send_message_confirm_tags[i]) == 44:
        final_send_message_confirm_tags.append(send_message_confirm_tags[i])
        final_send_message_sentences.append(send_message_sentences[i])
final_sentences = final_send_message_sentences + list(list_sentences.keys())
t_sentences = final_send_message_confirm_tags + make_call_tags

print(len(final_sentences), len(t_sentences), len(send_message_confirm_tags), len(send_message_sentences),
      len(final_send_message_confirm_tags), len(final_send_message_sentences))

import random

combined = list(zip(final_sentences, t_sentences))
random.shuffle(combined)
final_sentences[:], t_sentences[:] = zip(*combined)

import random
import numpy as np
from keras.layers import Embedding, Conv1D, MaxPooling1D, LSTM, GRU, Dense

train = final_sentences[:600]
test = final_sentences[600:]
train2_tags = final_sentences[:600]
test2_tags = final_sentences[600:]
# tags1=list(first_tags.values())
train1_y = t_sentences[:600]
test1_y = t_sentences[600:]
from numpy import array
from keras.utils import to_categorical
import numpy

y1 = numpy.zeros(shape=(600, 44, 3))

for i, v in enumerate(train1_y):
    a = np.array(v)
    # print(i)
    # y = [to_categorical(i, num_classes=0) for i in [v]]
    y = np.zeros((44, 3))

    y[np.arange(44), a] = 1
    y = [arr.tolist() for arr in y]

    y = array(y)
    # numpy.append(y1, y)
    y1[i] = y

y2 = numpy.zeros(shape=(159, 44, 3))
for i, v in enumerate(test1_y):
    a = np.array(v)

    # y = [to_categorical(i, num_classes=0) for i in [v]]
    y = np.zeros((44, 3))

    y[np.arange(44), a] = 1
    y = [arr.tolist() for arr in y]
    y = array(y)
    # numpy.append(y2, y)
    y2[i] = y

from numpy import array
import nltk
from nltk import FreqDist
import numpy as np

X_train = [nltk.word_tokenize(x) for x in train]
X_test = [nltk.word_tokenize(x) for x in test]

x_distr = FreqDist(np.concatenate(X_train + X_test))
x_vocab = x_distr.most_common(min(len(x_distr), 10000))

x_idx2word = [word[0].lower() for word in x_vocab]

x_idx2word.insert(0, '<PADDING>')
x_idx2word.append('<UNK>')

x_word2idx = {word: idx for idx, word in enumerate(x_idx2word)}

x_train_seq = np.zeros((len(X_train), 44), dtype=np.int32)
for i, da in enumerate(X_train):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 44:
            break
        token = token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_train_seq[i][j] = x_word2idx[token]
        else:
            x_train_seq[i][j] = x_word2idx['<UNK>']

x_test_seq = np.zeros((len(X_test), 44),
                      dtype=np.int32)  # padding implicitly present, as the index of the padding token is 0

# form embeddings for samples testing data
for i, da in enumerate(X_test):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 44:
            break
        token = token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_test_seq[i][j] = x_word2idx[token]
        else:
            x_test_seq[i][j] = x_word2idx['<UNK>']

import pickle

with open('x_word2idx_recepient.pickle', 'wb') as handle:
    pickle.dump(x_word2idx, handle, protocol=2)

from keras.layers.wrappers import Bidirectional
from keras.layers import Embedding
from keras.models import Model
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Dense
import numpy as np
from keras.layers import TimeDistributed, Dropout
# from keras_contrib.layers import CRF
from keras.layers import Conv2D

cnn_filters = 256  # number of filters in the convolutional layer
cnn_kernel_size = 3  # kernel size of the convolution
cnn_pool_size = 1
import keras


def train_neural_model(x_vocab, x_train_seq, y_train_seq, train2_tags):
    print(x_train_seq.shape, y_train_seq.shape)
    input1 = Input(shape=(44,))
    input2 = Input(shape=(30,))
    # build neural model
    embedding = Embedding(6000, 300, input_length=44)(input1)
    embedding2 = Embedding(300, 30, input_length=30)(input2)
    # embedding = keras.layers.concatenate([embedding1, embedding2])

    cnn = Conv1D(filters=cnn_filters, kernel_size=cnn_kernel_size, padding='same', activation='relu')(embedding)
    pool = MaxPooling1D(pool_size=cnn_pool_size)(cnn)

    # lstm1 = Bidirectional(LSTM(units=256,dropout=0.2,recurrent_dropout=0.1,return_sequences=True))(embedding)
    lstm1 = Bidirectional(LSTM(units=200, dropout=0.2, return_sequences=True, recurrent_dropout=0.6))(embedding)
    dp = Dropout(0.6)(lstm1)
    lstm2 = Bidirectional(LSTM(units=200, dropout=0.2, return_sequences=True, recurrent_dropout=0.6))(lstm1)
    lstm3 = Bidirectional(LSTM(units=200, return_sequences=True, recurrent_dropout=0.6))(lstm2)
    # lstm4 = Bidirectional(LSTM(units=200, return_sequences=True, recurrent_dropout=0.6))(lstm3)
    # output = Dense(3,activation='softmax')(lstm2)

    output = TimeDistributed(Dense(3, activation="softmax"))(pool)
    # crf = CRF(3)  # CRF layer
    # out = crf(output)
    lstm3 = Bidirectional(LSTM(units=256,
                               dropout=0.2,
                               recurrent_dropout=0.2,
                               return_sequences=False),
                          merge_mode='concat')(embedding)
    output2 = Dense(1, activation='sigmoid')(lstm3)
    model = Model(inputs=input1, outputs=output)
    # model2 = Model(inputs=input, outputs=output2)
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'],
                  weighted_metrics={1: 1, 0: 0.01, 2: 0.5})

    model.summary()

    model.fit(x_train_seq, y_train_seq, epochs=5, verbose=1, validation_split=0.2)
    real_recepient_model_json = model.to_json()
    with open("real_recepient_model_json.json", "w") as json_file:
        json_file.write(json.dumps(json.loads(real_recepient_model_json), indent=4))

    # serialize weights to HDF5
    model.save_weights("real_recepient_model_json.h5")


    return model


# evaluation of the model
def neural_model_eval(model, x_test_seq, y_test_seq):
    acc = model.evaluate(x_test_seq, y_test_seq)
    # print("Accuracy of the neural model is:", acc[1] * 100)
    predicted = model.predict(x_test_seq)
    # print("See predicted indices", predicted[0])
    # print("Compare it to the actual label indices",y_test_seq)

    # acc2 = model2.evaluate(x_test_seq, test2_tags)
    # print("Accuracy of the neural model is:", acc2[1] * 100)
    # predicted2 = model2.predict(x_test_seq)
    # print("See predicted indices", predicted2)
    return predicted


model = train_neural_model(x_vocab, x_train_seq, y1, train2_tags)

predicted = neural_model_eval(model, x_test_seq, y2)

i = 0
count_R = 0
right_R = 0
for i in range(len(x_test_seq)):
    actual = [np.argmax(y) for y in y2[i]]
    pred = [np.argmax(y) for y in predicted[i]]
    if 1 in pred and 1 in actual:
        act_ind = actual.index(1)
        pred_ind = pred.index(1)
        count_R = count_R + 1

        if act_ind == pred_ind:
            right_R = right_R + 1

    i = i + 1
print(count_R, right_R)



