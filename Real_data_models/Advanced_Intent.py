import bson, json
import nltk

with open('actionable_semantics.bson', 'rb') as f:
    data = bson.decode_all(f.read())

with open('user_query.bson', 'rb') as f:
    data2 = bson.decode_all(f.read())

with open('qu_state.bson', 'rb') as f:
    data3 = bson.decode_all(f.read())

final_sentences5=[]
for values in data2[:1000000]:
     if 'remove' in values['utterances'][0]['utterance'] or "delete" in values['utterances'][0]['utterance']:
        final_sentences5.append(values['utterances'][0]['utterance'])

final_dict = {}
for m, values in enumerate(data3):
    if values['slots']:
        for i, v in enumerate(values['slots']):
            if values['slots'][i]['origin_type'] == 'ADD_CONTACT_NAME':
                if "add" in data2[m]['utterances'][0]['utterance'] or "also" in data2[m]['utterances'][0]['utterance']:
                    # if len(values['slots'][0]['value'].split(" "))==1:
                    # print(len(values['slots'][i]['value'].split(" ")))

                    if len(values['slots'][i]['value'].split(" ")) == 1:
                        if values['slots'][i]['value'].split(" ")[0] in nltk.word_tokenize(
                                data2[m]['utterances'][0]['utterance'].lower()):
                            # print(m,nltk.word_tokenize(data2[m]['utterances'][0]['utterance'].lower()),values['slots'][i]['value'])
                            l = nltk.word_tokenize(data2[m]['utterances'][0]['utterance'].lower()).index(
                                values['slots'][i]['value'])


                            # print(data2[m]['utterances'][0]['utterance'],nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])
                        if data2[m]['utterances'][0]['utterance'] in final_dict:
                            final_dict[data2[m]['utterances'][0]['utterance']].append(
                                nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])
                        else:
                            final_dict[data2[m]['utterances'][0]['utterance']] = []
                            final_dict[data2[m]['utterances'][0]['utterance']].append(
                                nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])
                    else:
                        values1 = values['slots'][i]['value'].split(" ")
                        for v in values1:

                            if v in nltk.word_tokenize(data2[m]['utterances'][0]['utterance'].lower()):
                                l = nltk.word_tokenize(data2[m]['utterances'][0]['utterance'].lower()).index(v)
                                # print(m,data2[m]['utterances'][0]['utterance'],nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])
                                if data2[m]['utterances'][0]['utterance'] in final_dict:
                                    final_dict[data2[m]['utterances'][0]['utterance']].append(
                                        nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])
                                else:
                                    final_dict[data2[m]['utterances'][0]['utterance']] = []
                                    final_dict[data2[m]['utterances'][0]['utterance']].append(
                                        nltk.word_tokenize(data2[m]['utterances'][0]['utterance'])[l])

for key,value in final_dict.items():
    final_dict[key]=list(set(final_dict[key]))

final_tags = []
for key, value in final_dict.items():
    tags = [0] * 30

    for v in final_dict[key]:
        ind = nltk.word_tokenize(key).index(v)
        tags[ind] = 1

    final_tags.append(tags)

final_sentences4=list(final_dict.keys())

t_sentences4=[]
for i in range(140):
    t_sentences4.append(2)
t_sentences5=[]
for i in range(142):
    t_sentences5.append(3)

j=0
make_call_sentences=[]
for i,values in enumerate(data):
    if values['user_intent']=='MAKE_CALL' and values['machine_action']=='confirm':
        sentence=data2[i]['utterances'][0]['utterance'].lower()
        make_call_sentences.append(sentence)
        j=j+1

j = 0
l = []
for values in data2:
    if 'add' in values['utterances'][0]['utterance'] or 'remove' in values['utterances'][0]['utterance'] or 'also' in \
            values['utterances'][0]['utterance'] or 'Also' in values['utterances'][0]['utterance'] or "Don't" in \
            values['utterances'][0]['utterance'] or 'Add' in values['utterances'][0]['utterance']:

        i = i + 1

        l.append(values['utterances'][0]['utterance'])
    j = j + 1


send_confirm = []
indices = []
send_confirm_recipient = []
send_confirm_message = []

prev = ''
for i, values in enumerate(data):

    if values['user_intent'] == "SEND_MESSAGE" and values['machine_action'] == "confirm" and prev != values['user_id']:
        prev = values['user_id']
        if data2[i]['utterances'][0]['utterance'] not in send_confirm and data2[i]['utterances'][0][
            'utterance'] not in l:
            if "tell" in data2[i]['utterances'][0]['utterance'] or "send" in data2[i]['utterances'][0][
                'utterance'] or "Send" in data2[i]['utterances'][0]['utterance'] or "let" in data2[i]['utterances'][0][
                'utterance'] or "Tell" in data2[i]['utterances'][0]['utterance']:
                indices.append(i)
                import ast

                a = ast.literal_eval(values['data']['payload'])

                # for values1 in data3:
                # if values1['timestamp']==values['timestamp'] and values1['session_id']==values['session_id']:

                for v in data3[i]['slots']:

                    if v['type'] == 'CONTACT_NAME':

                        send_confirm.append(data2[i]['utterances'][0]['utterance'])
                        send_confirm_recipient.append(v['raw_value'])
                        send_confirm_message.append(a['message_value'])


send_request = []
indices = []
# send_confirm_recipient=[]
send_request_message = []

i = 0
for i, values in enumerate(data):
    if values['user_intent'] == "SEND_MESSAGE" and values['machine_action'] == "request":
        if data2[i]['utterances'][0]['utterance'] not in send_request and data2[i]['utterances'][0][
            'utterance'] not in l:

            indices.append(i)

            for values1 in data3:
                if values1['timestamp'] == values['timestamp'] and values1['session_id'] == values['session_id']:
                    for v in values1['slots']:
                        if v['type'] == 'MESSAGE_BODY':

                            send_request.append(data2[i]['utterances'][0]['utterance'])
                            send_request_message.append(v['raw_value'])
                            break

                    break



final_sentences=send_confirm+send_request
t_sentences1=[0]*2857

t_sentences2=[1]*723

final_sentences3=final_sentences+make_call_sentences+final_sentences4+final_sentences5
t_sentences3=t_sentences1+t_sentences2+t_sentences4+t_sentences5


import random
from keras.layers import Embedding, Conv1D, MaxPooling1D, LSTM, GRU, Dense

combined = list(zip(final_sentences3, t_sentences3))
random.shuffle(combined)

final_sentences3[:], t_sentences3[:] = zip(*combined)

train = final_sentences3[:3300]
test = final_sentences3[3300:]
train2_tags = final_sentences3[:3300]
test2_tags = final_sentences3[3300:]
train1_y = t_sentences3[:3300]
test1_y = t_sentences3[3300:]

from numpy import array
from keras.utils import to_categorical
from keras.utils import np_utils

# y = [to_categorical(i, num_classes=0) for i in train1_y]

# y2 = [to_categorical(i, num_classes=0) for i in test1_y]
# y2 = array(y2)
# y = array(y)
y = np_utils.to_categorical(train1_y, num_classes=4)
y2 = np_utils.to_categorical(test1_y, num_classes=4)

import nltk
from nltk import FreqDist
import numpy as np

X_train = [nltk.word_tokenize(x) for x in train]
X_test = [nltk.word_tokenize(x) for x in test]

x_distr = FreqDist(np.concatenate(X_train + X_test))
x_vocab = x_distr.most_common(min(len(x_distr), 10000))

x_idx2word = [word[0].lower() for word in x_vocab]

x_idx2word.insert(0, '<PADDING>')
x_idx2word.append('<UNK>')

x_word2idx = {word: idx for idx, word in enumerate(x_idx2word)}

x_train_seq = np.zeros((len(X_train), 44), dtype=np.int32)
for i, da in enumerate(X_train):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 44:
            break
        token = token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_train_seq[i][j] = x_word2idx[token]
        else:
            x_train_seq[i][j] = x_word2idx['<UNK>']

x_test_seq = np.zeros((len(X_test), 44),
                      dtype=np.int32)  # padding implicitly present, as the index of the padding token is 0

# form embeddings for samples testing data
for i, da in enumerate(X_test):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 44:
            break
        token = token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_test_seq[i][j] = x_word2idx[token]
        else:
            x_test_seq[i][j] = x_word2idx['<UNK>']

import pickle

with open('x_word2idx_intent.pickle', 'wb') as handle:
    pickle.dump(x_word2idx, handle, protocol=2)

from keras.layers.wrappers import Bidirectional
from keras.layers import Embedding
from keras.models import Model
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Dense

cnn_filters = 256  # number of filters in the convolutional layer
cnn_kernel_size = 6  # kernel size of the convolution
cnn_pool_size = 1


def train_neural_model(x_vocab, x_train_seq, y_train_seq, train2_tags):
    input = Input(shape=(44,))
    # build neural model
    embedding = Embedding(3000, 300, input_length=44)(input)
    cnn = Conv1D(filters=cnn_filters, kernel_size=cnn_kernel_size, padding='same', activation='relu')(embedding)
    pool = MaxPooling1D(pool_size=cnn_pool_size)(cnn)
    lstm3 = Bidirectional(LSTM(units=200,
                               dropout=0.6,
                               recurrent_dropout=0.6,
                               return_sequences=False),
                          merge_mode='concat')(embedding)
    output2 = Dense(4, activation='sigmoid')(lstm3)

    model2 = Model(inputs=input, outputs=output2)

    model2.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'],
                   weighted_metrics={1: 0.1, 0: 0.1, 2: 1.5})

    # train the model

    model2.summary()
    model2.fit(x_train_seq, train2_tags, epochs=5, verbose=1, validation_split=0.2)
    advanced_intent_model_json = model2.to_json()
    with open("real_intent_model_json.json", "w") as json_file:
        json_file.write(json.dumps(json.loads(advanced_intent_model_json), indent=4))

    # serialize weights to HDF5
    model2.save_weights("advanced_intent_model_json.h5")
    print("Saved model to disk")

    return model2


# evaluation of the model

model = train_neural_model(x_vocab, x_train_seq, x_test_seq, y)


def neural_model_eval(model2, x_test_seq, y_test_seq):
    acc2 = model2.evaluate(x_test_seq, y_test_seq)
    print("Accuracy of the neural model is:", acc2[1] * 100)
    predicted2 = model2.predict(x_test_seq)

    return predicted2

predicted = neural_model_eval(model, x_test_seq, test1_y)

