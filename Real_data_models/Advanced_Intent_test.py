import numpy as np
import pickle
import nltk
from keras.models import model_from_json
def check_add(input1):
    json_file3 = open('real_intent_model_json.json', 'r')
    loaded_model_json3 = json_file3.read()
    json_file3.close()
    loaded_model3 = model_from_json(loaded_model_json3)
    print("Loaded model from disk")
    loaded_model3.load_weights("advanced_intent_model_json.h5")





    x_word2idx_add_contacts = pickle.load(open("x_word2idx_intent.pickle", "rb"))

    def get_encoding(test):

        test = test.split()

        value = np.zeros((1, 44), dtype=np.int32)

        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 44:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx_add_contacts:
                value[0][j] = x_word2idx_add_contacts[token]
            else:
                value[0][j] = x_word2idx_add_contacts['<UNK>']
        return value

    x_test_seq = get_encoding(input1)
    print(x_test_seq)


    predicted = loaded_model3.predict(x_test_seq)
    print(predicted[0])
    #for i in range(len(x_test_seq)):

    pred = list(predicted[0]).index(max(list(predicted[0])))
    intent = ''
    if pred == 0:
        intent = 'SEND_MESSAGE'
    elif pred == 1:
        intent = 'MAKE_CALL'
    elif pred == 2:
        intent = 'ADD_CONTACT'
    else:
        intent = 'DELETE_CONTACT'
    return intent


