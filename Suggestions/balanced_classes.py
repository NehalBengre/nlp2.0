import random
import pandas as pd
from keras.layers import Embedding, Conv1D, MaxPooling1D, LSTM, GRU, Dense
import json
import random
import numpy as np
np.random.seed(1234)
# _RRN_ = recipient's name
# _RRG_ = recipient's gender
# _MMM_ = message
cols = ['names', 'freq', 'cumul_freq', "pos"]

second_tags = {}
comm = {}
options = [
    "please",
    "hurry and",
    "go ahead and",
    "do me a favor and"
]

commands = [
    "tell _RRN_ that_OPT _MMM_ .",
    "can you tell _RRN_ that_OPT _MMM_ ?",
    "can you please tell _RRN_ that_OPT _MMM_ ?",
    "send a message to _RRN_ to say that_OPT _MMM_ .",
    "ping _RRN_ to tell _RRG_ that_OPT _MMM_ .",
    "shoot a message to _RRN_ that_OPT _MMM_ .",
    "send _RRN_ that_OPT _MMM_ .",
    "let _RRN_ know that_OPT _MMM_ .",
    "could you please send a message to _RRN_ that_OPT _MMM_ ?",
    "could you send a message to _RRN_ that_OPT _MMM_ ?",
    "can you please send a message to _RRN_ that_OPT _MMM_ ?",
    "can you send a message to _RRN_ that_OPT _MMM_ ?",
    "I need _RRN_ to know that_OPT _MMM_ .",
    "Would you mind telling _RRN_ that_OPT _MMM_ .",
    "Go ahead and tell _RRN_ that_OPT _MMM_ .",
    "I need to tell _RRN_ that_OPT _MMM_ .",
    "I need _RRN_ to know that_OPT _MMM_ .",
    "_RRN_ needs to know that_OPT _MMM_ .",
    "it would be great if _RRN_ knew that_OPT _MMM_ .",
    "notify _RRN_ that_OPT _MMM_ .",
    "inform _RRN_ that_OPT _MMM_ .",
    "contact _RRN_ to tell _RRG_ that_OPT _MMM_ .",
    "message _RRN_ that_OPT _MMM_ ."
]

messages = [
    "I will be late",
    "I won't be coming tonight",
    "I am stuck in traffic",
    "dinner is now at 9:00pm",
    "I cannot be there for the meeting tomorrow",
    "_RRO_ won't be coming for dinner",
    "I need _RRO_ to pick me up from work soon",
    "my kids are sick and I need to work from home today",
    "I will call him back later",
    "we need need sheet in room _NUM_",
    "table _NUM_ is asking for the desserts menu",
    "table _NUM_ is getting impatient",
    "we need a cleanup aisle _NUM_",
    "the customer is very unhappy",
    "I need security on floor _NUM_",
    "I need help in section _NUM_",
    "room _NUM_ hasn't checked out yet",
    "I need fresh towels for room _NUM_, and fast",
    "there is an emergency on floor _NUM_",
    "_RRO_ might need some extra help cleaining room _NUM_",
    "I have an urgent message",
    "this is not very funny",
    "it's almost the end of the night shift",
    "I am so happy that it's almost the weekend",
    "the people in room _NUM_ seem to have forgotten a sweater",
    "the elevator is stuck again",
    "there is a situation on floor _NUM_",
    "the order for room _NUM_ isn't ready yet",
    "the customer in room _NUM_ is asking for more shampoo",
    "I am running out of shampoo on floor _NUM_",
    "there is a leak on floor _NUM_",
    "I need immediate backup on floor _NUM_",
    "it would be great if I could have some backup soon.",
    "I will need help to lift the bed in room _NUM_",
    "I need more cleaning products in room _NUM_",
    "the air-conditioning in room _NUM_ is out of service",
    "we will need an electrician on floor _NUM_",
    "the lock in room _NUM_ seems damaged",
    "we are out of tea bags on floor _NUM_",
    "_RRO_ is asking for help in room _NUM_",
    "_RRO_ is late and won't be there for the beginning of the next shift",
    "I can't believe how messy people in room _NUM_ are",
    "the lightbulb in room _NUM_ needs to be changed",
    "there is a power outage on floor _NUM_",
    "we need a first aid kit on floor _NUM_",
    "we need a spare key for the closet on floor _NUM_",
    "the carpet in room _NUM_ needs to be cleaned today",
    "I need new pillows for room _NUM_"
]

number_commands = {}
actual_commands = {}
for v in commands:
    v1 = v.split(" ")
    if v1.index("_RRN_") not in number_commands:
        number_commands[v1.index("_RRN_")] = 1
        actual_commands[v1.index("_RRN_")] = []
        actual_commands[v1.index("_RRN_")].append(v)
    else:
        number_commands[v1.index("_RRN_")] = number_commands[v1.index("_RRN_")] + 1
        actual_commands[v1.index("_RRN_")].append(v)
number_messages = {}
actual_messages = {}
for v in messages:
    v1 = v.split(" ")
    if len(v1) not in number_messages:
        number_messages[len(v1)] = 1
        actual_messages[len(v1)] = []
        actual_messages[len(v1)].append(v)
    else:
        number_messages[len(v1)] = number_messages[len(v1)] + 1
        actual_messages[len(v1)].append(v)
import random

final_sentences = []
t_sentences = []
for i in range(100):
    a = random.choice(actual_commands[0])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[10])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[1])
    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[11])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[2])
    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[4])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[3])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[5])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[4])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[6])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[5])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[7])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[6])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[8])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[j:(len(m) + j)] = [2] * len(m)
    t_sentences.append(total_indices)

    a = random.choice(actual_commands[7])

    j = a.split(" ").index("_MMM_")
    m = random.choice(actual_messages[9])
    a = a.replace("_OPT _MMM_", " " + m)
    m = m.split(" ")
    final_sentences.append(a)
    total_indices = [0] * 30
    total_indices[0:len(m)] = [2] * len(m)
    t_sentences.append(total_indices)

    #############################################################################################

    a = random.choice(actual_messages[8])

    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[9])

    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[10])

    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")

    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[11])
    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[12])

    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[14])

    m = random.choice(actual_commands[0])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    #####################################################################################################

    a = random.choice(actual_messages[9])

    m = random.choice(actual_commands[6])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[10])

    m = random.choice(actual_commands[6])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[11])

    m = random.choice(actual_commands[6])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[7])

    m = random.choice(actual_commands[6])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    a = random.choice(actual_messages[11])

    m = random.choice(actual_commands[7])
    m = m.replace("_OPT _MMM_", "")

    final_sentences.append(a + ", " + m)
    a = a.split(" ")
    total_indices = [0] * 30
    total_indices[0:len(a)] = [2] * len(a)
    t_sentences.append(total_indices)

    # a=random.choice(actual_messages[10])+" go ahead and "+random.choice(actual_commands[7]).replace("_OPT _MMM_","")
    # final_sentences.append(a)




DF_names_F = pd.read_csv("census-dist-female-first.txt", names=cols, sep="\s+")
# DF_names_F=['Richa','Alicia','Asha']
# DF_names_M=['Amar','Adarsh','Kian']
DF_names_M = pd.read_csv("census-dist-male-first.txt", names=cols, sep='\s+')

names_F = DF_names_F['names'].tolist()
# names_F=['Richa','Alicia','Asha']
# names_M=['Amar','Adarsh','Kian']
names_M = DF_names_M['names'].tolist()

indices = []
num = ["178", "289", "332", "400"]
sentences = []
for i, values in enumerate(final_sentences):
    indices.append(values.split(" ").index("_RRN_"))
    if "_NUM_" in values:
        final_sentences[i] = values.replace("_NUM_", random.choice(num))
for i, values in enumerate(final_sentences):
    if random.random() < 0.5:
        recipient = random.choice(names_M).capitalize()
        gender = "him"
        final_sentences[i] = values.replace("_RRN_", recipient).replace("_RRG_", gender)

        sentences.append(final_sentences[i])
    else:
        recipient = random.choice(names_F).capitalize()
        gender = "her"

        final_sentences[i] = values.replace("_RRN_", recipient).replace("_RRG_", gender)

for i, values in enumerate(final_sentences):
    if random.random() < 0.5:
        recipient = random.choice(names_M).capitalize()

        final_sentences[i] = values.replace("_RRO_", recipient)

        sentences.append(final_sentences[i])
    else:
        recipient = random.choice(names_F).capitalize()

        final_sentences[i] = values.replace("_RRO_", recipient)
for i, values in enumerate(final_sentences):
    t_sentences[i][indices[i]] = 1

#randomize
import random


combined = list(zip(final_sentences, t_sentences))
random.shuffle(combined)

final_sentences[:], t_sentences[:] = zip(*combined)

train=final_sentences[:1800]
test=final_sentences[1800:]
train2_tags=final_sentences[:1800]
test2_tags=final_sentences[1800:2000]
#tags1=list(first_tags.values())
train1_y=t_sentences[:1800]
test1_y=t_sentences[1800:]
from numpy import array
from keras.utils import to_categorical
import numpy
y1 = numpy.zeros(shape=(1800, 30, 3))
for i,v in enumerate(train1_y):
    y = [to_categorical(i, num_classes=0) for i in [v]]
    y = [arr.tolist() for arr in y]
    y = array(y)
    #numpy.append(y1, y)
    y1[i]=y

y2 = numpy.zeros(shape=(100, 30, 3))
for i,v in enumerate(test1_y):
    y = [to_categorical(i, num_classes=0) for i in [v]]
    y = [arr.tolist() for arr in y]
    y = array(y)
    #numpy.append(y2, y)
    y2[i]=y



from numpy import array
import nltk
from nltk import FreqDist
import numpy as np

X_train = [nltk.word_tokenize(x) for x in train]
X_test = [nltk.word_tokenize(x) for x in test]

x_distr = FreqDist(np.concatenate(X_train + X_test))
x_vocab = x_distr.most_common(min(len(x_distr), 10000))

x_idx2word = [word[0].lower() for word in x_vocab]

x_idx2word.insert(0, '<PADDING>')
x_idx2word.append('<UNK>')

x_word2idx = {word: idx for idx, word in enumerate(x_idx2word)}

x_train_seq = np.zeros((len(X_train), 30), dtype=np.int32)
for i, da in enumerate(X_train):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 30:
            break
        token=token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_train_seq[i][j] = x_word2idx[token]
        else:
            x_train_seq[i][j] = x_word2idx['<UNK>']

x_test_seq = np.zeros((len(X_test), 30),dtype=np.int32)  # padding implicitly present, as the index of the padding token is 0

    # form embeddings for samples testing data
for i, da in enumerate(X_test):
    for j, token in enumerate(da):
        # truncate long Titles
        if j >= 30:
            break
        token=token.lower()
        # represent each token with the corresponding index
        if token in x_word2idx:
            x_test_seq[i][j] = x_word2idx[token]
        else:
            x_test_seq[i][j] = x_word2idx['<UNK>']


from keras.layers.wrappers import Bidirectional
from keras.layers import Embedding
from keras.models import Model
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Dense
import numpy as np
from keras.layers import TimeDistributed,Dropout
#from keras_contrib.layers import CRF
from keras.layers import Conv2D

cnn_filters = 256                        # number of filters in the convolutional layer
cnn_kernel_size = 3              # kernel size of the convolution
cnn_pool_size = 1
import keras
def train_neural_model(x_vocab, x_train_seq, y_train_seq):
    print(y_train_seq.shape)
    input = Input(shape=(30,))
    input2 = Input(shape=(30,))
    # build neural model
    embedding = Embedding(6000, 300, input_length=30)(input)

    #embedding = keras.layers.concatenate([embedding1, embedding2])

    cnn=Conv1D(filters=cnn_filters, kernel_size=cnn_kernel_size, padding='same', activation='relu')(embedding)
    pool=MaxPooling1D(pool_size=cnn_pool_size)(cnn)

    #lstm1 = Bidirectional(LSTM(units=256,dropout=0.2,recurrent_dropout=0.1,return_sequences=True))(embedding)
    lstm1 = Bidirectional(LSTM(units=200, dropout=0.2,return_sequences=True, recurrent_dropout=0.6))(embedding)
    dp = Dropout(0.6)(lstm1)
    lstm2 = Bidirectional(LSTM(units=200, dropout=0.2,return_sequences=True, recurrent_dropout=0.6))(lstm1)
    lstm3 = Bidirectional(LSTM(units=200, return_sequences=True, recurrent_dropout=0.6))(lstm2)
    #lstm4 = Bidirectional(LSTM(units=200, return_sequences=True, recurrent_dropout=0.6))(lstm3)
    #output = Dense(3,activation='softmax')(lstm2)

    output = TimeDistributed(Dense(3, activation="softmax"))(pool)
    #crf = CRF(3)  # CRF layer
    #out = crf(output)
    lstm3 = Bidirectional(LSTM(units=256,
                               dropout=0.2,
                               recurrent_dropout=0.2,
                               return_sequences=False),
                          merge_mode='concat')(embedding)
    output2 = Dense(1, activation='sigmoid')(lstm3)
    model = Model(inputs=input, outputs=output)
    #model2 = Model(inputs=input, outputs=output2)
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'],weighted_metrics={1: 1, 0: 0.01,2:0.5})
    #model.compile(optimizer="rmsprop", loss=crf.loss_function, metrics=[crf.accuracy])
    #model2.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])

    # train the model
    model.summary()
    #model2.summary()
    #from keras_contrib.utils import save_load_utils
    filepath = 'trained_model.hdf5'
    #save_load_utils.save_all_weights(model,filepath)
    #checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    #callbacks_list = [checkpoint]

    model.fit(x_train_seq, y_train_seq, epochs=5, verbose=1, validation_split=0.2)
    #model2.fit(x_train_seq, train2_tags, epochs=5, verbose=1, validation_split=0.2)
    #save_load_utils.save_all_weights(model, filepath)
    model_json = model.to_json()
    with open("model2.json", "w") as json_file:
        json_file.write(json.dumps(json.loads(model_json), indent=4))

    # serialize weights to HDF5
    model.save_weights("model2.h5")
    print("Saved model to disk")

    return model


# evaluation of the model
def neural_model_eval(model,  x_test_seq, y_test_seq, test2_tags):
    print(len(x_test_seq),len(y_test_seq))
    acc = model.evaluate([x_test_seq], y_test_seq)
    #print("Accuracy of the neural model is:", acc[1] * 100)
    predicted = model.predict([x_test_seq])
    #print("See predicted indices", predicted[0])
    # print("Compare it to the actual label indices",y_test_seq)

    #acc2 = model2.evaluate(x_test_seq, test2_tags)
    #print("Accuracy of the neural model is:", acc2[1] * 100)
    #predicted2 = model2.predict(x_test_seq)
    #print("See predicted indices", predicted2)
    return predicted


model = train_neural_model(x_vocab, x_train_seq, y1)

predicted=neural_model_eval(model,x_test_seq,y2,test2_tags)

for i in range(len(x_train_seq)):
    actual = [np.argmax(y) for y in y2[i]]
    print(actual)
for i in range(len(x_test_seq)):
    actual = [np.argmax(y) for y in y2[i]]
    print(actual)


i = 0
count=0
right=0
print(len(y2))
for i in range(len(x_test_seq)):
    actual = [np.argmax(y) for y in y2[i]]
    pred=[np.argmax(y) for y in predicted[i]]
    #print(test[i],actual,pred)

    #if 1.0 in pred:
        #act_ind=actual.index(1)
        #pred_ind=pred.index(1.0)
        #count=count+1
        #print(pred_ind,act_ind)
        #if act_ind==pred_ind:
            #right=right+1




    i = i + 1
#print(count,right)





