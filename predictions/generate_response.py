import random
import string

#templated responses
INCOMING_MESSAGE_READ={"confirm":["_SENDER_ sent you a message. Do you want me to open it for you?","Got a message from _SENDER_ . Do you want me to read it?","Incoming message from _SENDER_ .Do you want me to read it?"],
                       "execute":["This is what _SENDER_ said. _MESSAGE_ .","Message from _SENDER_ said _MESSAGE_ ."]}

MAKE_CALL={"find":["Sorry, cannot find that user or group name. Can you please spell it?"],
           "confirm":["I am about to make a call to _RECEPIENT_ . Do you confirm?","Okay, calling now. Do you confirm?","Do you want to call _RECEPIENT_?"],
           "execute":["Alright, making your call.","Alright, calling them now"],
           "request":["Who are you trying to call?You should specify a recepient"],
           "select":["I have _NUMBER_ contacts for you _RECEPIENT_LIST_ .Who do you want to call?","Here are _NUMBER_ contacts for you._RECEPIENT_LIST_ .Who do you want me to call?"],
           "other":["Sorry, I am not able to find _RECEPIENT_ in your contact list","Sorry, there is no _RECEPIENT_ in your contact list."]}

SEND_MESSAGE={"find":["Sorry, cannot find that user or group name. Can you please spell it?"],
              "confirm":["You are about to send a message to _RECEPIENT_ . Ready to send it?","Are you sure you want to send a message to _RECEPIENT_ .","Sending your message to _RECEPIENT_ . Do you confirm?","Do you want your message to _RECEPIENT_ to be sent?"],
              "confirm_list": ["You are about to send a message to _RECEPIENTLIST_ .Ready to send it?"],
              "execute":['Okay. I am sending your message','Alright, sending your message','I am sending your message.'],
              "request":['Who is the recepient of this message','Who would you like to send a message to'],
              "select":["I have _NUMBER_ contacts for you _RECEPIENT_LIST_ . Please select one of those options","Here are _NUMBER_ contacts for you _RECEPIENT_LIST_ . Who do you want me to message?"],
              "other":["Sorry, I am not able to find _RECEPIENT_ in your contact list","It does not seem you have _RECEPIENT_ in your contacts"]}


def generate_MAKE_CALL_responses(actual_recepient,recepient_list,users_data,machine_action,session):
    data = {}
    user_response = ''
    print("machine_action",machine_action)

    recepient_id = users_data['recepient_id']
    print("last_stage", users_data)
    message = users_data['message']

    print("machine_action", machine_action)
    if machine_action == "request":

        user_response = random.choice(MAKE_CALL[machine_action])
        data = {'encoding': 'json',
                'payload': {"protocol_request": False, "message_request": False, "contact_request": True},
                'schema': 'message_request_data'}
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'MAKE_CALL',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}


    elif machine_action=="confirm":
        user_response = random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_", recepient_list[0])

        if len(recepient_list)==1:

            if users_data['group']==False:

                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "contact_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                                    "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
            else:

                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "group_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                                    "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
                print("DATAAA",data)

        else:
            if users_data['group'] == False:

                list_recipients=[]
                i=0
                for values in recepient_id:
                    temp =[]
                    temp={"type": "contact_id", "value": values}, {"value": actual_recepient[i], "type": "name"},{"type": "protocol", "value": "mobile"}
                    list_recipients.append(temp)
                data = {'encoding': 'json',
                        'payload': {"contact_value": [list_recipients
                            ], "protocol_value": "mobile",
                            "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
            else:

                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "group_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                            "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
                print("DATAAA", data)


    elif machine_action=="select":

        user_response = random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_LIST_", ', '.join(recepient_list))
        user_response = user_response.replace("_NUMBER_", str(len(recepient_list)))

        data = {'encoding': 'json',
                'payload': {"contact_option": [
                        ]
                         , "protocol_options": [],
                                "message_options": []},
                'schema': 'message_select_data'}

        #data["payload"]["contact_option"] = []
        for values in recepient_list:
            data["payload"]["contact_option"].append({"type": "name", "value": values})
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'MAKE_CALL',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}

    elif machine_action=="execute":
        print("here")
        user_response = random.choice(MAKE_CALL[machine_action])

        if users_data['group'] == False:
            type = "contact_id"

        else:
            type = "group_id"

        data = {'encoding': 'json',
                'payload': {"contact_value": [
                    [{"type": type, "value": recepient_id}, {"type": "name", "value": actual_recepient},
                     {"type": "protocol", "value": "mobile"}]],
                            "message_value": user_response},
                'schema': 'message_execute_data'}
        final_data = {"data": data, "user_id": recepient_id, "timestamp": users_data['timestamp'],
                      "user_intent": 'MAKE_CALL',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}

    else:
        user_response = random.choice(MAKE_CALL[machine_action])
        user_response = user_response.replace("_RECEPIENT_", actual_recepient)
        data = {'data': {}}
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'MAKE_CALL',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}


    final_data = {"data":data,"user_id": users_data['user_id'], "timestamp": users_data['timestamp'], "user_intent": 'MAKE_CALL',
              "user_response": user_response, 'session_id': session,'machine_action': machine_action}
    print("MAKE_CALL EXECUTE", final_data)

    return final_data

def generate_INCOMING_MESSAGE_READ_responses(sender_name,users_data,machine_action,session,utterances,audio_id):
    data = {}
    user_response = ''
    print("USERS",users_data)
    user_response = random.choice(INCOMING_MESSAGE_READ[machine_action])
    user_response = user_response.replace("_SENDER_", sender_name)

    if machine_action=="confirm":
        user_response = user_response.replace("_SENDER_", sender_name)
        message = users_data['message_body'].split("|")[0]
        print("MESSAGE",message)
        #audio = users_data['message_body'].split("|")[1]
        #url = "https://audio.smartear.ai/retrieving_message?clipping_job_id=" + audio

        data = {'encoding': 'json',
                'payload': {"protocol_value":"mobile","from_user_id": users_data['contact_id'], "message_value": message, "audio_id": audio_id,
                            "from_session_id":0,"contact_value": [[{"type": "name", "value": sender_name}, {"type": "contact_id", "value": users_data["contact_id"]}]]},
                'schema': 'message_execute_data'}

    elif machine_action=="execute":

        user_response = utterances
        data = {'encoding': 'json',
                'payload': {"protocol_value": "mobile", "contact_value": [[{"value": users_data['sender_id'], "type": "contact_id"}, {"value": sender_name, "type": "name"},{"value": "mobile", "type": "protocol"}]],
                            "message_value": utterances},
                'schema': 'message_execute_data'}

    final_data = {"data": data,  "timestamp": users_data['timestamp'],
                  "user_intent": 'INCOMING_MESSAGE_READ',
                  "user_response": user_response, 'session_id': session,
                  'machine_action': machine_action,"user_id":users_data['user_id']}
    print("FINAL_DATA IN INCOMING_MESSAGE",final_data)

    return final_data


def generate_SEND_MESSAGE_responses(actual_recepient,recepient_list,users_data,machine_action,session):
    data={}
    user_response=''

    print("last_stage",users_data)
    if machine_action == "request":
        pass
    else:
        message = users_data['message']

    print("machine_action",machine_action)
    if machine_action=="request":

        user_response = random.choice(SEND_MESSAGE[machine_action])
        data = {'encoding': 'json',
                'payload': {"protocol_request": False, "message_request": False, "contact_request": True},
                'schema': 'message_request_data'}
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'SEND_MESSAGE',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}




    elif machine_action=="confirm":
        recepient_id = users_data['recepient_id']
        user_response = random.choice(SEND_MESSAGE["confirm"])
        user_response = user_response.replace("_RECEPIENT_", recepient_list[0])
        print(recepient_list)
        if len(recepient_list)==1:

            if users_data['group']==False:

                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "contact_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                                    "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
            else:

                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "group_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                                    "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
                print("DATAAA",data)

        else:
            user_response = random.choice(SEND_MESSAGE["confirm_list"])
            user_response = user_response.replace("_RECEPIENTLIST_", ', '.join(recepient_list))

            if users_data['group'] == False:

                list_recipients=[]
                i=0
                for values in actual_recepient:
                    temp =[]
                    temp={"type": "contact_id", "value": recepient_id[i]}, {"value": values, "type": "name"},{"type": "protocol", "value": "mobile"}
                    list_recipients.append(temp)
                data = {'encoding': 'json',
                        'payload': {"contact_value": [list_recipients
                            ], "protocol_value": "mobile",
                            "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
            else:

                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "group_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                            "message_value": message},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}
                print("DATAAA", data)
        print("DATA",data)
        machine_action="confirm"
        final_data = {"data": data, "user_id": recepient_id, "timestamp": users_data['timestamp'],
                      "user_intent": 'SEND_MESSAGE',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}


    elif machine_action=="select":
        recepient_id = users_data['recepient_id']
        user_response = random.choice(SEND_MESSAGE[machine_action])
        user_response = user_response.replace("_RECEPIENT_LIST_", ', '.join(recepient_list))
        user_response = user_response.replace("_NUMBER_", str(len(recepient_list)))

        data = {'encoding': 'json',
                'payload': {"contact_option": [
                        ]
                         , "protocol_options": [],
                                "message_options": []},
                'schema': 'message_select_data'}

        #data["payload"]["contact_option"] = []
        for values in recepient_list:
            data["payload"]["contact_option"].append({"type": "name", "value": values})
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'SEND_MESSAGE',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}

    elif machine_action=="execute":
        print("USERS_DATA",users_data,recepient_list)
        #audio = users_data['message_body'].split("|")[1]

        recepient_id = users_data['recepient_id']
        user_response = random.choice(SEND_MESSAGE[machine_action])
        user_response = user_response.replace("_RECEPIENT_", recepient_list[0])

        def id_generator(size=8, chars=string.ascii_letters):
            return ''.join(random.choice(chars) for _ in range(size))

        audio=id_generator()
        print("clipping_job in generate_response audio_id", audio)

        if len(recepient_list)>1:
            list_recipients = []
            i = 0
            for values in recepient_id:
                temp = []
                temp = [{"type": "contact_id", "value": values}, {"value": actual_recepient[i], "type": "name"}, {
                    "type": "protocol", "value": "mobile"}]
                list_recipients.append(temp)

            if users_data['group'] == False:
                data = {'encoding': 'json',
                        'payload': {"contact_value": list_recipients
                                                      , "protocol_value": "mobile",
                                    "message_value": message,"audio_id":audio,"message_body_timestamp": users_data["first_timestamp"]},
                        "message_value": user_response,
                        'schema': 'message_execute_data'}


            else:
                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": list_recipients, "protocol_value": "mobile",
                            "message_value": message,"audio_id":audio,"message_body_timestamp": users_data["first_timestamp"]},
                        'schema': 'message_execute_data',
                        "message_value": user_response}
            final_data = {"data": data, "user_id": recepient_id,
                          "user_intent": 'SEND_MESSAGE',
                          "user_response": user_response, 'session_id': session, 'machine_action': machine_action
                          }

        else:
            if users_data['group'] == False:

                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "contact_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                            "message_value": message,"audio_id":audio,"message_body_timestamp": users_data["first_timestamp"]},
                        'schema': 'message_execute_data',
                        "message_value": user_response}
            else:
                recepient_id = recepient_id
                data = {'encoding': 'json',
                        'payload': {"contact_value": [
                            [{"type": "group_id", "value": recepient_id}, {"value": actual_recepient, "type": "name"},
                             {"type": "protocol", "value": "mobile"}]], "protocol_value": "mobile",
                            "message_value": message,"audio_id":audio,"message_body_timestamp": users_data["first_timestamp"]},
                        'schema': 'message_execute_data',
                        "message_value": user_response}


            recepient_id = users_data['recepient_id']
            print("here")

            final_data = {"data": data, "user_id": recepient_id,
                          "user_intent": 'SEND_MESSAGE',
                          "user_response": user_response, 'session_id': session, 'machine_action': machine_action}




    else:
        recepient_id = users_data['recepient_id']
        user_response = random.choice(SEND_MESSAGE[machine_action])
        print("actual_recepient",actual_recepient)
        user_response = user_response.replace("_RECEPIENT_", actual_recepient)
        data = {'data': {}}
        final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                      "user_intent": 'ABORT',
                      "user_response": user_response, 'session_id': session, 'machine_action': machine_action}


    print("FINAL_DATA", final_data)
    return final_data

def generate_CANCEL_MESSAGE_responses(actual_recepient, recepient_list, users_data, machine_action, session):
    user_response="No problem, I am going to cancel the message"
    data = {'data': {}}
    final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                  "user_intent": 'ABORT',
                  "user_response": user_response, 'session_id': session, 'machine_action': machine_action}
    return final_data

def generate_CANCEL_CALL_responses(actual_recepient, recepient_list, users_data, machine_action, session):
    user_response="No problem, I am not going to make the call"
    data = {'data': {}}
    final_data = {"data": data, "user_id": users_data['user_id'], "timestamp": users_data['timestamp'],
                  "user_intent": 'ABORT',
                  "user_response": user_response, 'session_id': session, 'machine_action': machine_action}
    return final_data



