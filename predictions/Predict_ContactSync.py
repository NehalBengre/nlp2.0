import os
import pickle
import random
import sys
import re

import nltk
import numpy as np
from keras.models import model_from_json

from flask import g
import pymongo as pymongo

sys.path.append("..")
import generate_response
#import Add_contact_utils as Add_contact_utils
import Contact_matching as Contact_matching
import Delete_contact_utils as Delete_contact_utils
import recep_message
import advanced_intent
from pymongo import MongoClient
from g2p_en import g2p
import flask
from flask import request
import generate_phonemes as generate_phonemes

np.random.seed(1234)

client = MongoClient('localhost', 27017)
db = client['NLP2']

user_requests = db.user_requests
timeline = db.timeline

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = None


def load_model_and_weights(model_json_fname, weights_fname, model_name=""):
    assert (model_json_fname.endswith(".json"))
    with open(model_json_fname, 'r') as json_file:
        model_json = json_file.read()
    model = model_from_json(model_json)
    print("Loaded model from disk " + model_name)
    assert (weights_fname.endswith(".h5"))
    model.load_weights(weights_fname)
    print("Loaded model weights from disk " + model_name)
    return model


@app.before_request
def add_server_to_globals():
    print(("* Loading Keras models and Flask starting server..."
           "please wait until server has fully started"))

    # TODO have a list of absolute paths for the models, instead of changing directories
    os.chdir("../Real_data_models/")

    # Load Recepient model
    loaded_model = load_model_and_weights(
        model_json_fname='advanced_recepient_model_json.json',
        weights_fname='advanced_recepient_model_json.h5',
        model_name="recepient"
    )
    g.loaded_model = loaded_model

    # Load Intent model
    loaded_model2 = load_model_and_weights(
        model_json_fname='advanced_intent_model_json.json',
        weights_fname='advanced_intent_model_json.h5',
        model_name="intent"
    )
    g.loaded_model2 = loaded_model2

    os.chdir("../predictions")


@app.route("/v1/health")
def health():
    key = request.args.get('key')
    if key == "c536K5L7XSuRKbWZid5CMUafjhIcNG9J":

        response = {"success": True}
        message = ""
        response["message"] = "You could connect succesfully here"
    else:
        response = {"success": False}
        message = ""
        response["message"] = "You need proper authorization to perform this operation"
    return flask.jsonify(response)


@app.route("/v1/contact-sync", methods=["POST"])
def modification():
    key = request.args.get('key')
    if key == "c536K5L7XSuRKbWZid5CMUafjhIcNG9J":
        response = {"success": False}

        final_data = request.json
        print("final_data", final_data)
        db.logs.insert(final_data)
        print("inserted final_data")

        id = final_data["id"]

        if final_data['type'] == "group":

            if final_data['mode'] == "create":
                phoneme = generate_pre_phoneme(final_data['name'], True)
                print(phoneme, final_data['name'])
                new_data = {'id': final_data['id'], 'created': final_data['modified'], "name": final_data['name'],
                            "team": final_data['team'], "phoneme": phoneme}
                db.groups.insert(new_data)

            elif final_data['mode'] == "modify":
                phoneme = generate_pre_phoneme(final_data['name'], True)
                db.groups.update_one(
                    {"id": final_data["id"]},
                    {
                        "$set": {
                            "created": final_data['modified'],
                            "name": final_data['name'],
                            "team": final_data['team'],
                            "phoneme": phoneme
                        }
                    }
                )
            elif final_data['mode'] == "delete":

                group = final_data['id']

                group_deleted = db.contact_groups.find_one({"id": group})
                id_list = group_deleted['id_list']
                db.contact_groups.delete_one({"id": group})
                db.groups.delete_one({"id": group})

            response = {"success": True, "message": "The group record is updated"}

        elif final_data['type'] == "user":

            if final_data['mode'] == "modify":

                groups_list = []
                admin_users_list = []

                # updating users
                print(final_data)
                if 'name' in final_data.keys():
                    print("true")
                    phonemes_list = generate_pre_phoneme(final_data, False)
                    print("phonemes_list", phonemes_list)
                    db.contacts_users.update_one(
                        {"uid": final_data["id"]},
                        {
                            "$set": {
                                "teaminfo": final_data['teaminfo'],
                                "username": final_data['name'],
                                "email": final_data['email'],
                                "nickname": final_data['nickname'],
                                "names_phonemes": phonemes_list

                            }
                        }
                    )
                else:
                    db.contacts_users.update_one(
                        {"uid": final_data["id"]},
                        {
                            "$set": {
                                "teaminfo": final_data['teaminfo'],
                                "email": final_data['email'],
                                "nickname": final_data['nickname']

                            }
                        }
                    )

                print("completed syncing with contact_users")

                for values in final_data['teaminfo'].keys():
                    if 'groups' in final_data['teaminfo'][values]:
                        admin_users_list += final_data['teaminfo'][values]['groups'].values()
                        groups_list += list(final_data['teaminfo'][values]['groups'].keys())

                print(admin_users_list, groups_list)
                # generating phonemes
                # phonemes_list = generate_pre_phoneme(final_data['name'], False)


                # updating contact-groups

                for values in groups_list:
                    prev_users = db.contact_groups.find_one({"id": values})
                    print(values, "previous", prev_users)
                    if prev_users is not None:
                        if id not in prev_users['id_list']:
                            prev_users['id_list'].append(id)
                            prev_users['list_users'].append(final_data['name'])
                        elif id in prev_users['id_list']:
                            prev_index = prev_users['id_list'].index(id)
                            prev_users['list_users'][prev_index] = final_data['name']
                        db.contact_groups.update_one(
                            {"id": values},
                            {
                                "$set": {
                                    "list_users": prev_users['list_users'],
                                    "id_list": prev_users['id_list']
                                }
                            }
                        )
                    else:

                        group = db.groups.find_one({"id": values})

                        new_group = {"created": group['created'], "id": group['id'], "team": group['team'],
                                     "id_list": [final_data["id"]], "list_users": [final_data['name']]}
                        db.contact_groups.insert(new_group)

                print("completed syncing with contact_groups")

            elif final_data['mode'] == "create":

                groups_list = []
                admin_users_list = []

                for values in final_data['teaminfo']:
                    if 'groups' in final_data['teaminfo'][values]:
                        admin_users_list += final_data['teaminfo'][values]['groups'].values()
                        groups_list += list(final_data['teaminfo'][values]['groups'].keys())

                if 'name' in final_data.keys():
                    phonemes_list = generate_pre_phoneme(final_data['name'], False)
                    new_data = {"uid": final_data['id'], "username": final_data['name'], "email": final_data['email'],
                                "teaminfo": final_data['teaminfo'],
                                "created": final_data['created'], "names_phonemes": phonemes_list}
                    print("phonems", phonemes_list)
                else:
                    new_data = {"uid": final_data['id'], "email": final_data['email'],
                                "teaminfo": final_data['teaminfo'],
                                "created": final_data['created'], "modified": final_data['modified']}

                db.contacts_users.insert(new_data)

                for values in groups_list:

                    prev_users = db.contact_groups.find_one({"id": values})

                    if prev_users != None:
                        if id not in prev_users['id_list']:
                            prev_users['id_list'].append(id)
                            prev_users['list_users'].append(final_data['name'])

                        elif id in prev_users['id_list']:
                            prev_index = prev_users['id_list'].index(id)
                            prev_users['list_users'][prev_index] = final_data['name']
                        db.contact_groups.update_one(
                            {"id": values},
                            {
                                "$set": {
                                    "list_users": prev_users['list_users'],
                                    "id_list": prev_users['id_list']
                                }
                            }
                        )
                    else:
                        group = db.groups.find_one({"id": values})
                        new_group = {"created": group['created'], "id": group['id'], "team": group['team'],
                                     "id_list": [id], "list_users": [final_data['name']]}
                        db.contact_groups.insert(new_group)

            elif final_data['mode'] == "delete":
                groups_list = []
                admin_users_list = []
                print("id", id)
                user = db.contacts_users.find_one({"uid": id})

                for values in user['teaminfo'].keys():
                    if 'groups' in user['teaminfo'][values]:
                        admin_users_list += user['teaminfo'][values]['groups'].values()
                        groups_list += list(user['teaminfo'][values]['groups'].keys())
                for values in groups_list:
                    prev_users = db.contact_groups.find_one({"id": values})
                    if id in prev_users['id_list']:
                        del prev_users['id_list'][prev_users['id_list'].index(id)]
                        del prev_users['list_users'][prev_users['list_users'].index(user['username'])]
                        db.contact_groups.update_one(
                            {"id": values},
                            {
                                "$set": {
                                    "list_users": prev_users['list_users'],
                                    "id_list": prev_users['id_list']
                                }
                            }
                        )

                db.contacts_users.delete_one({"uid": id})

        response = {"success": True, "message": "The user record is updated"}



    else:
        response = {"success": False}
        message = ""
        response["message"] = "You need proper authorization to perform this operation"

    return flask.jsonify(response)


def generate_pre_phoneme(finaldata, group):
    if group == True:
        phonemes_list = generate_phonemes.grapheme_to_phoneme(finaldata, True)
    else:
        temp = []

        if 'name' in finaldata:
            user_split = finaldata['name'].split(" ")
            temp.append(user_split[0])
            if len(user_split) > 1:
                temp.append(user_split[1])
            temp.append(finaldata['name'])
        if 'nickname' in finaldata:
            temp.append(finaldata['nickname'])
        print("temp", temp)
        phonemes_list = generate_phonemes.grapheme_to_phoneme(temp, False)
    print("Phonemes_list", phonemes_list)
    return phonemes_list


y = []


def grapheme_to_phoneme(test_list, group):
    result = []
    if group == True:
        inter = g2p(test_list)
        result.append(inter)
    else:
        for values in test_list:
            inter = g2p(values)
            inter = give_result(inter)
            result.append(inter)
    print("result", result)
    return result


def give_result(text):
    result = []
    final_result = []
    for values in text:
        S = ''
        for d in values:

            if d.isdigit() == False and d != ' ':
                S = S + d

        result.append(S)
    for values in result:
        if values is not '':
            final_result.append(values)
    return final_result


@app.errorhandler(502)
def error_502(e):
    response = {"success": False, "message": "There was some issue with the NLP Server. Try again later"}
    return flask.jsonify(response)


@app.errorhandler(500)
def error_500(e):
    response = {"success": False, "message": "Invalid data sent. 500"}
    return flask.jsonify(response)


@app.errorhandler(400)
def error_400(e):
    response = {"success": False, "message": "Invalid data sent. 400"}
    return flask.jsonify(response)


def add_state(machine_action, receipent_list, id, time, timeline_data):
    print("TIME", timeline_data)
    latest_id = db.state.find_one({"user_id": timeline_data[0]['user_id']}, sort=[('id', pymongo.DESCENDING)])
    print(latest_id)
    print("RECIPENT_LIST", receipent_list)

    if machine_action == '' or machine_action == "new":
        machine_action = 'new'
        if latest_id != None:
            latest_id = latest_id['id'] + 1
        else:
            latest_id = 1

        if machine_action == "add_contact":

            # new_state = {"machine_action":machine_action,"receipent_list":prev_receipient_list+receipent_list, "id":latest_id}
            new_state = {"machine_action": machine_action, "receipent_list": receipent_list,
                         "id": latest_id['id'], "timestamp": timeline_data[0]['timestamp'],
                         "session": timeline_data[0]['session_id'], "user_id": timeline_data[0]['user_id']}
        else:
            new_state = {"machine_action": machine_action, "receipent_list": receipent_list,
                         "id": latest_id, "first_timestamp": time[0]['timestamp'],
                         "timestamp": timeline_data[0]['timestamp'], "session": timeline_data[0]['session_id'],
                         "user_id": timeline_data[0]['user_id']}
    else:
        if machine_action == "add_contact":

            # new_state = {"machine_action":machine_action,"receipent_list":prev_receipient_list+receipent_list, "id":latest_id['id']}
            new_state = {"machine_action": machine_action, "receipent_list": receipent_list,
                         "id": latest_id['id'], "timestamp": timeline_data[0]['timestamp'],
                         "session": timeline_data[0]['session_id'], "user_id": timeline_data[0]['user_id']}
        else:
            new_state = {"machine_action": machine_action, "receipent_list": receipent_list,
                         "id": latest_id['id'], "timestamp": timeline_data[0]['timestamp'],
                         "session": timeline_data[0]['session_id'], "user_id": timeline_data[0]['user_id']}
    print("NEW STATE", new_state)
    db.state.insert(new_state)


@app.route("/v1/predict", methods=["POST"])
def predict():
    response = {"success": False}
    input = request.json
    print("INPUT SENT", input)

    ####insert in server logs
    db.logs.insert(input)
    ## TODO insert the input in the beginnning in order to debug bad requests

    loaded_model = g.loaded_model
    loaded_model2 = g.loaded_model2

    utterance = input['utterances'][0]['utterance']
    final_data = {"predictions": {}, "success": False}
    if "." in utterance and not utterance.endswith("."):
        print("had .")
        utterance = utterance.replace(".", " that")
        print(utterance)

    recepient, message = recep_message.check_recepient(utterance, loaded_model)
    print(recepient, message)

    intent = advanced_intent.check_intent(utterance, loaded_model2)
    print(intent)

    if intent == 'ADD_CONTACT':
        add_state("add_contact", recepient, '', '', [input])

    elif intent == 'DELETE_CONTACT':
        add_state("add_contact", recepient, '', '', [input])

    final_data = pre_check(input,intent)
    ## TODO replace model, model2 etc with recepient, intent model, etc.

    # check for intent-change
    print("RESULT_AFTER FINAL_DATA", final_data)

    if final_data == None:

        # corner cases
        if intent == "MAKE_CALL" and recepient == []:
            print("MAKE_CALL missing recepient")
            final_data = request.json
            session_id = final_data["session_id"]
            timestamp = final_data["timestamp"]
            user_id = final_data["user_id"]
            utterances = final_data["utterances"][0]["utterance"]

            user_requests.insert(final_data)

            machine_action = "request"
            time1 = [
                {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                 "machine_action": "request", "intent": intent, "recipients": []}]
            result = timeline.insert(time1)

            users_data = {"session_id": session_id, "timestamp": timestamp, "user_id": user_id,
                          "message": ' '.join(message), "recepient_id": []}
            add_state("request", [], '', '', time1)

            final_data = generate_response.generate_MAKE_CALL_responses([], [], users_data,
                                                                        machine_action, session_id)

            # user_requests.insert(final_data)
            response["success"] = True
            # return the data dictionary as a JSON response
            # response["response"]=final_data
            final_data["success"] = True

            return flask.jsonify(final_data)
        if recepient == [] and message == []:
            user_response = "Your message is missing recipient and message, try sending a proper message later"
            data = {'data': {}}
            session_id = input["session_id"]
            timestamp = input["timestamp"]
            user_id = input["user_id"]
            utterances = input["utterances"][0]["utterance"]
            time1 = [
                {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                 "machine_action": "other", "intent": intent, "recipients": []}]
            result = timeline.insert(time1)
            final_data = {"data": {}, "user_id": '', "timestamp": input['timestamp'],
                          "user_intent": 'ABORT',
                          "user_response": user_response, 'session_id': input['session_id'], 'machine_action': "other"}
            final_data["success"] = True
            add_state("other", [], '', '', time1)
            return flask.jsonify(final_data)

        if recepient is not [] and message == [] and intent == "SEND_MESSAGE":
            user_response = "You have not specified a message to send to" + recepient
            data = {'data': {}}
            session_id = input["session_id"]
            timestamp = input["timestamp"]
            user_id = input["user_id"]
            utterances = input["utterances"][0]["utterance"]

            session = random.randint(1000000000, 10000000000)
            time1 = [{"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "new", "intent": intent}]
            result = timeline.insert(time1)
            add_state("new", [], '', time1, time1)

            time1 = [
                {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                 "machine_action": "request", "intent": intent, "recipients": []}]
            result = timeline.insert(time1)
            final_data = {"data": {}, "user_id": '', "timestamp": input['timestamp'],
                          "user_intent": 'SEND_MESSAGE',
                          "user_response": user_response, 'session_id': input['session_id'],
                          'machine_action': "request"}
            final_data["success"] = True
            add_state("request", recepient[0], '', '', time1)
            return flask.jsonify(final_data)

        if recepient == []:
            final_data = request.json
            session_id = input["session_id"]
            timestamp = input["timestamp"]
            user_id = input["user_id"]
            utterances = input["utterances"][0]["utterance"]
            user_requests.insert(final_data)
            session = random.randint(1000000000, 10000000000)
            time1 = [{"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "new", "intent": intent}]
            result = timeline.insert(time1)
            add_state("new", [], '', time1, time1)
            machine_action = "request"
            time1 = [
                {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                 "machine_action": "request", "intent": intent, "recipients": []}]
            result = timeline.insert(time1)

            users_data = {"session_id": session_id, "timestamp": timestamp, "user_id": user_id,
                          "message": ' '.join(message)}
            add_state("request", [], '', '', time1)

            final_data = generate_response.generate_SEND_MESSAGE_responses(recepient, [], users_data,
                                                                           machine_action, session_id)

            response["success"] = True
            final_data["success"] = True

            return flask.jsonify(final_data)

        print("user_id", input['user_id'])

        # corner cases end

        recepient_list = []
        is_group = []
        select_recepients = False
        for individual_recepients in recepient:
            print("individyal", individual_recepients, grapheme_to_phoneme([recepient], False))
            recepient_matched, is_group_matched = Contact_matching.match_contacts(individual_recepients,
                                                                                  grapheme_to_phoneme(
                                                                                      [individual_recepients], False),
                                                                                  input['user_id'])
            if len(recepient_matched) == 1:
                if len(recepient_matched[0]) == 1:
                    print("v true", recepient_matched)
                    recepient_list.append(recepient_matched)
                else:
                    recepient_list.append(recepient_matched[0])
                is_group.append(is_group_matched)
            else:
                select_recepients = True
                print(recepient_matched)
                for i, ind_receps in enumerate(recepient_matched):
                    recepient_list.append(ind_receps)
                    is_group.append(is_group_matched)

        print("RECIPIENT_LIST after make_call", recepient_list, select_recepients)

        final_data = request.json
        session_id = final_data["session_id"]
        timestamp = final_data["timestamp"]
        user_id = final_data["user_id"]
        utterances = final_data["utterances"][0]["utterance"]

        user_requests.insert(final_data)

        users_data = {"session_id": session_id, "timestamp": timestamp, "user_id": user_id}
        users_data['message'] = ' '.join(message)

        print("inserted values in user_requests database for logs")
        data = {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "utterances": utterances}

        result_timeline = db.timeline.find_one({"user_id": user_id, "session_id": session_id},
                                               sort=[('_id', pymongo.DESCENDING)])
        print("RESULT_TIMELINE HERE", result_timeline)
        recepient_id = ''
        if result_timeline == None:
            users_data['recepient_id'] = recepient_id
            session = random.randint(1000000000, 10000000000)
            print("Its a new response")
            time1 = [{"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "new", "intent": intent}]
            result = timeline.insert(time1)
            add_state("new", [], '', time1, time1)
            # fix 8
            latest_id = db.state.find_one({"user_id": user_id}, sort=[('_id', pymongo.DESCENDING)])
            db.state.update_one(
                {"_id": latest_id['_id']},
                {
                    "$set": {
                        "transcript": utterance
                    }
                }
            )

            if recepient_list == [] and recepient != None:
                recepient_id = ''
                machine_action = "other"
                time1 = [{"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                          "machine_action": "other", "intent": intent, "recipients": []}]
                result = timeline.insert(time1)
                add_state("other", [], '', '', time1)
                final_data = generate_response.generate_SEND_MESSAGE_responses(recepient[0], recepient_list, users_data,
                                                                               machine_action, session)
                return flask.jsonify(final_data)

            if recepient_list == [] and intent == "SEND_MESSAGE":
                recepient_id = ''
                machine_action = "request"
                time1 = [{"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                          "machine_action": "request", "intent": intent, "recipients": []}]
                result = timeline.insert(time1)
                add_state("request", [], '', '', time1)

            elif len(recepient_list) == 1:
                print("AFTER CONTACT MATCHING", recepient_list[0], is_group)
                session = random.randint(1000000000, 10000000000)
                if is_group[0] == True:
                    recepient_id = db.groups.find_one({"name": recepient_list[0]})
                    users_data['group'] = True
                    users_data['recepient_id'] = recepient_id['id']
                else:
                    recepient_id = db.contacts_users.find_one({"username": recepient_list[0]})
                    users_data['group'] = False
                    users_data['recepient_id'] = recepient_id['uid']
                # print(recepient_list, [recepient_id['id']])
                machine_action = "confirm"

                # FIX3
                if is_group[0] == False:
                    time1 = [
                        {"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": message,
                         "machine_action": "confirm", "intent": intent, "recipients": recepient_list,
                         "recipient_id": [recepient_id['uid']]}]
                # fix6
                else:
                    time1 = [
                        {"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": message,
                         "machine_action": "confirm", "intent": intent, "recipients": recepient_list,
                         "recipient_id": [recepient_id['id']]}]
                result = timeline.insert(time1)
                add_state("confirm", recepient_list, '', '', time1)

            elif select_recepients == False and len(recepient_list) > 1:
                print("AFTER CONTACT MATCHING", recepient_list, is_group)
                session = random.randint(1000000000, 10000000000)
                final_recepient_list = []
                final_recepient_id = []
                for i, values in enumerate(recepient_list):
                    if is_group[i] == True:
                        recepient_id = db.groups.find_one({"name": values})
                        users_data['group'] = True
                        users_data['recepient_id'] = recepient_id['id']
                        final_recepient_list.append(values)
                        final_recepient_id.append(recepient_id['id'])


                    else:
                        recepient_id = db.contacts_users.find_one({"username": values})
                        users_data['group'] = False
                        users_data['recepient_id'] = recepient_id['uid']
                        final_recepient_list.append(values)
                        final_recepient_id.append(recepient_id['uid'])

                        # print(recepient_list, [recepient_id['id']])
                time1 = [
                    {"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": message,
                     "machine_action": "confirm", "intent": intent, "recipients": final_recepient_list,
                     "recipient_id": final_recepient_id}]
                print("select_recipients", final_recepient_list, final_recepient_id)
                machine_action = "confirm"

                result = timeline.insert(time1)

                add_state("confirm", final_recepient_list, '', '', time1)

            elif len(recepient_list) > 0:
                session = random.randint(1000000000, 10000000000)
                print("i am actually here", ' '.join(message))
                recepient_id = ''
                machine_action = "select"
                time1 = [
                    {"session_id": session, "timestamp": timestamp, "user_id": user_id, "message": ' '.join(message),
                     "machine_action": "select", "intent": intent, "recipients": recepient_list}]
                add_state("select", [], '', '', time1)
                result = timeline.insert(time1)


        elif len(recepient_list) > 1:
            session = session_id
            recepient_id = ''
            machine_action = "select"
            time1 = [{"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "select", "intent": intent, "recipients": recepient_list}]
            add_state("confirm", recepient_list, '', '', time1)
            result = timeline.insert(time1)

        elif len(recepient_list) == 1:
            if is_group == True:
                recepient_id = db.groups.find_one({"name": recepient_list[0]})
            else:
                recepient_id = db.contacts_users.find_one({"username": recepient_list[0]})
            session = session_id
            machine_action = "confirm"

            time1 = [
                {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                 "machine_action": "confirm", "intent": intent, "recipients": recepient_list}]
            result = timeline.insert(time1)
            add_state("confirm", recepient_list, recepient_id, '', time1)

        elif result_timeline['machine_action'] == "confirm":
            session = session_id

            if len(recepient_list) == 1:
                if is_group == True:
                    recepient_id = db.groups.find_one({"name": recepient_list[0]})
                else:
                    recepient_id = db.contacts_users.find_one({"username": recepient_list[0]})
                print("in confirm", recepient_id)
                machine_action = "execute"
                time1 = [
                    {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": message,
                     "machine_action": "execute", "intent": intent, "recipients": recepient_id}]
                result = timeline.insert(time1)
                add_state("execute", ['recepient_id'], '', '', time1)

            elif len(recepient_list) == 0:
                session = session_id
                recepient_id = ''
                machine_action = "other"
                time1 = [
                    {"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                     "machine_action": "other", "intent": intent, "recipients": recepient_list}]
                result = timeline.insert(time1)
                add_state("other", recepient_list, '', '', time1)

        elif recepient == '':
            print(" in here after the changes")
            session = session_id
            machine_action = "request"
            time1 = [{"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "request", "intent": intent, "recipients": recepient_list}]
            result = timeline.insert(time1)
            add_state("request", recepient_list, '', '', time1)

        elif len(recepient_list) > 0:
            session = session_id
            machine_action = "select"
            time1 = [{"session_id": session_id, "timestamp": timestamp, "user_id": user_id, "message": utterances,
                      "machine_action": "select", "intent": intent, "recipients": recepient_list}]
            result = timeline.insert(time1)
            add_state("select", [], '', '', time1)

        print("USERS", users_data)
        if intent == "MAKE_CALL":
            final_data = generate_response.generate_MAKE_CALL_responses(recepient, recepient_list, users_data,
                                                                        machine_action, session)
        else:
            final_data = generate_response.generate_SEND_MESSAGE_responses(recepient, recepient_list, users_data,
                                                                           machine_action, session)
            if machine_action == "execute":
                print(" in here too")
                latest_id = db.state.find_one({"user_id": user_id}, sort=[('_id', pymongo.DESCENDING)])
                if type(result_timeline['message']) == list:
                    message = ' '.join(result_timeline['message'])
                else:
                    message = result_timeline['message']
                clipping_job = [
                    {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                     "_id": final_data['data']['payload']['audio_id'], "transcript": latest_id['transcript'],
                     "message": message
                     }]
                result = db.clipping_jobs.insert(clipping_job)
                add_state("execute", recepient_list, '', '', time1)

            return flask.jsonify(final_data)

        # user_requests.insert(final_data)
        response["success"] = True
        # return the data dictionary as a JSON response
        # response["response"]=final_data
        final_data["success"] = True

        return flask.jsonify(final_data)
    else:
        print("in here finally")
        # final_data["success"] = True
        # final_data_sent["message"]="Message successfully recieved!"

        return flask.jsonify(final_data)


def pre_check(input, intent):
    print("In pre-check")
    input1 = input['utterances'][0]['utterance']
    final_data = {"predictions": {}, "success": False}
    result_timeline = db.timeline.find_one({"user_id": input['user_id'], "session_id": input["session_id"]},
                                           sort=[('_id', pymongo.DESCENDING)])
    print("TIMELINE", result_timeline)
    incoming_potential_replies = ["Yes", "yes", "sure", "Sure", "Okay", "okay", "ok", "open", "Open"]
    if input1 in incoming_potential_replies:

        if result_timeline is not None and result_timeline['intent'] == "INCOMING_MESSAGE_READ":
            users_data = {}
            users_data = {"session_id": result_timeline["session_id"], "timestamp": result_timeline["timestamp"],
                          "sender_id": result_timeline['sender_id'], "user_id": result_timeline['user_id']}
            final_data = generate_response.generate_INCOMING_MESSAGE_READ_responses(result_timeline['sender_name'],
                                                                                    users_data,

                                                                                    "execute", input["session_id"],
                                                                                    result_timeline['message'],
                                                                                    result_timeline['audio_id'])
            return final_data

    intent_changed = False
    if result_timeline is not None and result_timeline['machine_action'] != 'select' and "call" in input1:

        new_intent = advanced_intent.check_intent(input['utterances'][0]['utterance'], g.loaded_model2)
        result_timeline = db.timeline.find_one({"user_id": input['user_id'], "session_id": input["session_id"]},
                                               sort=[('_id', pymongo.DESCENDING)])
        prev_intent = result_timeline['intent']
        print(prev_intent, new_intent, result_timeline['_id'])
        if new_intent != prev_intent:
            # db.timeline.find_one_and_update({"_id": result_timeline['_id']})
            print("timeline being changed", result_timeline)
            db.timeline.update_one(
                {"_id": result_timeline['_id']},
                {
                    "$set": {
                        "intent": new_intent
                    }
                }
            )
            intent_changed = True
            print("timeline changed", result_timeline)
        print("intent changed", new_intent)

    print(input["session_id"], result_timeline)

    #is_delete_result = Delete_contact_utils.is_delete(input1)
    #print("is delete_result", is_delete_result)
    if result_timeline == None:
        return None

    elif intent == "DELETE_CONTACT":
    #elif Add_contact_utils.check_state(result_timeline["user_id"]) == "delete_contact":

        print(" In delete")
        if result_timeline['machine_action'] == "confirm":
            rc_list = result_timeline['recipients']
            rc_list_id = result_timeline['recipient_id']
            print("rc_list", rc_list)
            latest_id = db.state.find_one({"user_id": result_timeline['user_id']}, sort=[('_id', pymongo.DESCENDING)])
            latest_id_delete = db.state.find_one(
                {"user_id": result_timeline['user_id'], 'id': latest_id['id'], 'machine_action': "delete_contact"},
                sort=[('_id', pymongo.DESCENDING)])
            result_contact_matching_names = []
            result_contact_matching_group = []

            print(latest_id_delete['receipent_list'])
            for values in latest_id_delete['receipent_list']:
                result_contact_matching = Contact_matching.match_contacts(values,
                                                                          grapheme_to_phoneme([values], False),
                                                                          input['user_id'])
                # print(result_contact_matching)
                result_contact_matching_names.extend(result_contact_matching[0])
                result_contact_matching_group.append(result_contact_matching[1])

            print("result_contact_matching", result_contact_matching_names)
            for values in result_contact_matching_names:
                if values not in rc_list:
                    print("not in list")
                else:
                    index_deleted = rc_list.index(values)
                    rc_list.remove(values)
                    del rc_list_id[index_deleted]

                print("RECIPIENTS_ID IN DELETE", rc_list_id)
                session = result_timeline['session_id']
                machine_action = "confirm"

            time1 = [
                {"session_id": session, "timestamp": result_timeline['timestamp'],
                 "user_id": result_timeline['user_id'], "message": result_timeline['message'],
                 "machine_action": "confirm", "intent": result_timeline['intent'], "recipient_id": rc_list_id,
                 "recipients": rc_list}]
            result = timeline.insert(time1)
            add_state("confirm", rc_list_id, 'list_recepient_id', '', time1)
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          'recepient_id': rc_list_id,
                          "user_id": input["user_id"], "message": result_timeline['message'], 'group': False}
            print(users_data, "USERS_DATA IN DELETE")

            if result_timeline['intent'] == "MAKE_CALL":
                final_data = generate_response.generate_MAKE_CALL_responses(rc_list_id, rc_list,
                                                                            users_data,
                                                                            "confirm", session)
            else:
                final_data = generate_response.generate_SEND_MESSAGE_responses(rc_list_id, rc_list,
                                                                               users_data,
                                                                               "confirm", session)

            return final_data

    elif intent == "ADD_CONTACT":
    #elif Add_contact_utils.check_state(result_timeline["user_id"]) == "add_contact":

        print(" In new add-contact", result_timeline)
        latest_id = db.state.find_one({"user_id": result_timeline['user_id']}, sort=[('_id', pymongo.DESCENDING)])
        if latest_id['receipent_list'] == []:
            recepient_id = ''
            session = result_timeline['session_id']
            machine_action = "request"
            time1 = [{"session_id": session, "timestamp": result_timeline['timestamp'],
                      "user_id": result_timeline['user_id'], "message": '',
                      "machine_action": "request", "intent": result_timeline['intent'], "recipients": []}]
            result = timeline.insert(time1)
            add_state("request", [], '', '', time1)
            users_data = {"session_id": session, "timestamp": result_timeline['timestamp'],
                          "user_id": result_timeline['user_id'], "message": ''}
            # users_data['message'] = ' '.join(message)
            final_data = generate_response.generate_SEND_MESSAGE_responses([], [], users_data,
                                                                           machine_action, session)
            return final_data

        if result_timeline['machine_action'] == "confirm":
            rc_list = result_timeline['recipients']
            latest_id = db.state.find_one({"user_id": result_timeline['user_id']}, sort=[('id', pymongo.DESCENDING)])
            latest_id_add = db.state.find_one(
                {"user_id": result_timeline['user_id'], 'id': latest_id['id'], 'machine_action': "add_contact"},
                sort=[('_id', pymongo.DESCENDING)])
            result_contact_matching_names = []
            result_contact_matching_group = []

            print("latest", latest_id_add['receipent_list'])

            if type(latest_id_add['receipent_list']) == list:
                for values in latest_id_add['receipent_list']:
                    print("VALUES in here", values, latest_id_add, grapheme_to_phoneme([values], False))
                    result_contact_matching = Contact_matching.match_contacts(values,
                                                                              grapheme_to_phoneme([values], False),
                                                                              input['user_id'])
                    print("here1", result_contact_matching)
                    if result_contact_matching[0] == []:
                        print("in here because no recipient")
                        recepient_id = ''
                        machine_action = "other"
                        time1 = [
                            {"session_id": result_timeline['session_id'], "timestamp": result_timeline['timestamp'],
                             "user_id": result_timeline['user_id'], "message": result_timeline['message'],
                             "machine_action": "other", "intent": result_timeline['intent'], "recipients": []}]
                        result = timeline.insert(time1)
                        add_state("other", [], '', '', time1)

                        time1 = [
                            {"session_id": result_timeline['session_id'], "timestamp": result_timeline['timestamp'],
                             "user_id": result_timeline['user_id'], "message": result_timeline['message'],
                             "machine_action": "other", "intent": result_timeline['intent'],
                             "recipients": []}]
                        result = timeline.insert(time1)

                        users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                                      'recepient_id': [],
                                      "user_id": input["user_id"], "message": result_timeline['message'],
                                      'group': False}

                        if result_timeline['intent'] == "MAKE_CALL":
                            final_data = generate_response.generate_MAKE_CALL_responses(
                                latest_id_add['receipent_list'][0],
                                [],
                                users_data,
                                "other", input["session_id"])
                        else:
                            final_data = generate_response.generate_SEND_MESSAGE_responses(
                                latest_id_add['receipent_list'][0],
                                [],
                                users_data,
                                "other",
                                input["session_id"])

                        return final_data

                    result_contact_matching_names.append(result_contact_matching[0][0])
                    result_contact_matching_group.append(result_contact_matching[1])

                print("result_contact_matching here", result_contact_matching_names, result_contact_matching)


            else:
                print(" I reached here")
                result_contact_matching = Contact_matching.match_contacts(latest_id_add['receipent_list'],
                                                                          grapheme_to_phoneme(
                                                                              [latest_id_add['receipent_list']], False),
                                                                          input['user_id'])
                result_contact_matching_names.append(result_contact_matching[0][0])
                result_contact_matching_group.append(result_contact_matching[1])
                print("this result_contact_matching", result_contact_matching_names, result_contact_matching)
                # print(db.timeline.find_one({"_id": result_timeline['_id']}))

                # db.timeline.find_one_and_update({"_id": result_timeline['_id']},

                # {"$set": {"recipients":latest_id['receipent_list'] }})
                print("result_contact_matching", result_contact_matching_names, result_contact_matching)
        print("LEN", len(result_contact_matching_names), len(result_contact_matching[0]))
        if len(result_contact_matching_names) == 1 and len(result_contact_matching[0]) > 1:
            print("In select1 for result_contact_matching", result_contact_matching_names)
            recepient_id_list = []
            recepient_id_list_names = []
            for i, values_rc_list in enumerate(result_contact_matching):
                print("NAME", result_contact_matching[0][i])
                if result_contact_matching[1] == "True":
                    recepient_id = db.groups.find_one({"name": result_contact_matching[0][i]})
                    recepient_id_list.append(recepient_id['id'])
                    recepient_id_list_names.append(recepient_id['username'])

                else:
                    recepient_id = db.contacts_users.find_one({"username": result_contact_matching[0][i]})
                    recepient_id_list.append(recepient_id['uid'])
                    recepient_id_list_names.append(recepient_id['username'])
            session = result_timeline['session_id']

            recepient_id = ''
            print("select_results", recepient_id_list_names)
            machine_action = "select"
            time1 = [{"session_id": result_timeline['session_id'], "timestamp": result_timeline['timestamp'],
                      "user_id": result_timeline['user_id'], "message": result_timeline['message'],
                      "machine_action": "select", "intent": result_timeline['intent'],
                      "recipients": recepient_id_list_names}]
            result = timeline.insert(time1)
            add_state("select", [], '', '', time1)
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          'recepient_id': recepient_id_list,
                          "user_id": input["user_id"], "message": result_timeline['message'], 'group': False}

            if result_timeline['intent'] == "MAKE_CALL":
                final_data = generate_response.generate_MAKE_CALL_responses(recepient_id_list, recepient_id_list_names,
                                                                            users_data,
                                                                            "select", session)
            else:
                final_data = generate_response.generate_SEND_MESSAGE_responses(recepient_id_list,
                                                                               recepient_id_list_names,
                                                                               users_data,
                                                                               "select", session)

            return final_data

        if len(result_contact_matching_names) == 1 and len(result_contact_matching[0]) == 1:
            print("In select for result_contact_matching", result_contact_matching_names, result_contact_matching[1])
            if result_contact_matching[1] == True:
                recepient_id = db.groups.find_one({"name": result_contact_matching[0][0]})

            else:
                recepient_id = db.contacts_users.find_one({"username": result_contact_matching[0][0]})
            session = result_timeline['session_id']
            # print(" Something seriously wrong here", result_timeline['recipient_id'], recepient_id['uid'])
            list_recepient_id = []
            print("If group", result_contact_matching[1])
            if type(result_timeline['recipient_id']) == list:
                for values in result_timeline['recipient_id']:
                    list_recepient_id.append(values)
                if result_contact_matching[1] == True:
                    print("In group")
                    list_recepient_id.append(recepient_id['id'])
                else:

                    list_recepient_id.append(recepient_id['uid'])
            else:
                print("in here since only 1")
                list_recepient_id.extend((result_timeline['recipient_id'], recepient_id['uid']))
            print(list_recepient_id)
            list_recepients = []
            if type(result_timeline['recipients']) == list:
                for values in result_timeline['recipients']:
                    list_recepients.append(values)
                list_recepients.append(result_contact_matching[0][0])
            else:
                list_recepients.extend((result_timeline['recipients'], result_contact_matching[0][0]))
            print(list_recepients)

            # print(" Something seriously wrong here",result_timeline['recipient_id'], recepient_id_list)
            # list_recepient_id = []
            #
            # if type(result_timeline['recipient_id'])==list:
            #     for values in result_timeline['recipient_id']:
            #
            #         list_recepient_id.append(values)
            #     list_recepient_id.append(recepient_id['uid'])
            # else:
            #     print("in here since only 1")
            #     list_recepient_id.extend((result_timeline['recipient_id'],recepient_id['uid']))
            # print(list_recepient_id)
            # list_recepients = []
            # if type(result_timeline['recipients'])==list:
            #     for values in result_timeline['recipients']:
            #         list_recepients.append(values)
            #     list_recepients.append(result_contact_matching[0][0])
            # else:
            #     list_recepients.extend((result_timeline['recipients'], result_contact_matching[0][0]))
            # print(list_recepients)


        # more than 1 recepients in add_contact

        else:

            recepient_id = []
            i = 0
            for values in result_contact_matching_names:
                print("VALUES", values)
                if result_contact_matching_group[i] == "True":
                    recepient_id1 = db.groups.find_one({"name": values})
                else:
                    recepient_id1 = db.contacts_users.find_one({"username": values})

                i = i + 1
                print(recepient_id1)
                recepient_id.append(recepient_id1['uid'])

            print("RECIPIENTS_ID IN ADD", recepient_id)
            session = result_timeline['session_id']
            machine_action = "confirm"
            list_recepient_id = []
            list_recepient_id.append(result_timeline['recipient_id'][0])
            list_recepient_id.extend(recepient_id)
            print(list_recepient_id)
            list_recepients = []
            list_recepients.append(result_timeline['recipients'][0])
            list_recepients.extend(result_contact_matching_names)
            print(list_recepients)
        time1 = [
            {"session_id": session, "timestamp": result_timeline['timestamp'], "user_id": result_timeline['user_id'],
             "message": result_timeline['message'],
             "machine_action": "confirm", "intent": result_timeline['intent'], "recipient_id": list_recepient_id,
             "recipients": list_recepients}]
        result = timeline.insert(time1)
        add_state("confirm", list_recepient_id, 'list_recepient_id', '', time1)
        users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                      'recepient_id': list_recepient_id,
                      "user_id": input["user_id"], "message": result_timeline['message'], 'group': False}

        if result_timeline['intent'] == "MAKE_CALL":
            final_data = generate_response.generate_MAKE_CALL_responses(list_recepient_id, list_recepients, users_data,
                                                                        "confirm", session)
        else:
            final_data = generate_response.generate_SEND_MESSAGE_responses(list_recepient_id, list_recepients,
                                                                           users_data,
                                                                           "confirm", session)

        return final_data

    ##made changes to include first,second.. on select
    elif result_timeline['machine_action'] == "select":
        rc_list = result_timeline['recipients']

        order = ["first", "second", "third", "fourth", "fifth"]
        order_3recipients = ["former", "middle", "latter"]
        order_2recipients = ["former", "latter"]

        select_names = {}
        for i, values_rc_list in enumerate(rc_list):

            if values_rc_list not in select_names.keys():
                select_names[values_rc_list] = []
                select_names[values_rc_list].append(values_rc_list.lower())
                word = values_rc_list.split(" ")

                for values in word:
                    select_names[values_rc_list].append(values.lower())

            else:
                select_names[values_rc_list].append(values_rc_list.split(" "))
            if (i == len(rc_list) - 1):
                select_names[values_rc_list].append("last")
            select_names[values_rc_list].append(order[i])
            if len(rc_list) == 3:
                select_names[values_rc_list].append(order_3recipients[i])
            elif len(rc_list) == 2:
                select_names[values_rc_list].append(order_2recipients[i])

        for key in select_names.keys():

            if set(input1.lower().split()) & set(select_names[key]):
                final_recipient = key

        print("final_recipent", final_recipient)
        final_recipient_list = [final_recipient]

        result_timeline = db.timeline.find_one({"user_id": input['user_id'], "machine_action": "select"},
                                               sort=[('_id', pymongo.DESCENDING)])
        print("RESULT_TIMELINE", result_timeline)
        message = result_timeline['message']

        recepient_id = db.contacts_users.find_one({"username": final_recipient})
        if recepient_id == None:
            is_group = True
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "message": message, 'group': True}

            recepient_id = db.groups.find_one({"name": final_recipient})
            users_data['recepient_id'] = recepient_id['id']
        else:
            is_group = False
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "message": message, 'group': False}

            recepient_id = db.contacts_users.find_one({"username": final_recipient})
            users_data['recepient_id'] = recepient_id['uid']
            print("RECIP", recepient_id['uid'])

        # fix7
        prev_timelines = db.timeline.find({'session_id': result_timeline['session_id']},
                                          sort=[('_id', pymongo.DESCENDING)])

        new_timelline = {}
        print("AFTER SELECTION", final_recipient_list, recepient_id)
        for iter in prev_timelines:
            if iter['machine_action'] == "confirm":
                new_timelline = iter
        print("prev_timelines", prev_timelines, new_timelline)
        if new_timelline != {}:

            final_receivers_list = new_timelline['recipients'] + final_recipient_list
            final_id_list = new_timelline['recipient_id'] + [recepient_id['uid']]
            if result_timeline['intent'] == "MAKE_CALL":
                time1 = [
                    {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                     "message": result_timeline['message'],
                     "machine_action": "confirm", "intent": result_timeline['intent'],
                     "recipients": final_receivers_list, "recipient_id": final_id_list}]

                result = timeline.insert(time1)
                add_state("confirm", [final_recipient], '', '', time1)
                final_data = generate_response.generate_MAKE_CALL_responses(
                    final_id_list, final_receivers_list, users_data,
                    "confirm", input["session_id"])
            else:
                time1 = [
                    {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                     "message": result_timeline['message'],
                     "machine_action": "confirm", "intent": result_timeline['intent'],
                     "recipients": final_receivers_list, "recipient_id": final_id_list}]

                result = timeline.insert(time1)
                add_state("confirm", [final_recipient], '', '', time1)
                print("FINAL_RECIPIENTS_LIST", new_timelline['recipient_id'] + final_recipient_list)
                print("ID", new_timelline['recipients'] + final_recipient_list)
                if is_group:
                    final_data = generate_response.generate_SEND_MESSAGE_responses(final_id_list, final_receivers_list,
                                                                                   users_data,
                                                                                   "confirm", input["session_id"])
                else:

                    final_data = generate_response.generate_SEND_MESSAGE_responses(
                        new_timelline['recipient_id'] + [recepient_id['uid']],
                        new_timelline['recipients'] + final_recipient_list, users_data,
                        "confirm", input["session_id"])

            return final_data
        else:
            time1 = [
                {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                 "message": result_timeline['message'],
                 "machine_action": "confirm", "intent": result_timeline['intent'], "recipients": [final_recipient],
                 "recipient_id": [recepient_id['uid']]}]
            print("input", input)
            result = timeline.insert(time1)
            add_state("confirm", [final_recipient], '', '', time1)
            if result_timeline['intent'] == "MAKE_CALL":
                final_data = generate_response.generate_MAKE_CALL_responses(recepient_id['uid'], final_recipient_list,
                                                                            users_data,
                                                                            "confirm", input["session_id"])
            else:
                if is_group:
                    final_data = generate_response.generate_SEND_MESSAGE_responses(recepient_id['id'],
                                                                                   final_recipient_list, users_data,
                                                                                   "confirm", input["session_id"])
                else:

                    final_data = generate_response.generate_SEND_MESSAGE_responses(recepient_id['uid'],
                                                                                   final_recipient_list, users_data,
                                                                                   "confirm", input["session_id"])

        return final_data

    elif result_timeline['machine_action'] == "confirm":
        positive = ["Sure", "Yep", "Yes", "Okay", "Send", "Great send now", "Yeah", "Read", "yes", "okay"]
        negative = ["No", "Don't", "don't", "cancel", "Cancel", "no"]
        first_timestamp = db.state.find_one({"user_id": result_timeline['user_id']}, sort=[('id', pymongo.DESCENDING)])[
            'first_timestamp']
        print("FIRST_ID", first_timestamp)

        print("message", input)
        result_timeline = db.timeline.find_one(
            {"_id": result_timeline['_id']})
        if input1 in positive:
            machine_action = "execute"

        print("recepient_id", result_timeline)
        users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input["user_id"],
                      "recepient_id": result_timeline['recipient_id'], "message": input["utterances"][0]["utterance"],
                      "first_timestamp": first_timestamp}
        print("users_data", users_data)

        # cancelling

        tokenized_input = nltk.word_tokenize(input1)

        if set(negative) & set(tokenized_input):
            machine_action = "other"

            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "recepient_id": input['user_id'],
                          "message": input["utterances"][0]["utterance"]}
            time1 = [
                {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                 "message": input["utterances"][0]["utterance"],
                 "machine_action": "other", "intent": result_timeline['intent'],
                 "recipients": []}]
            result = timeline.insert(time1)

            add_state("other", [input['user_id']], '', '', time1)
            if result_timeline['intent'] == "MAKE_CALL":
                final_data = generate_response.generate_CANCEL_CALL_responses(input1, [], users_data,
                                                                              "other", input["session_id"])
            else:
                final_data = generate_response.generate_CANCEL_MESSAGE_responses(input1, [], users_data,
                                                                                 "other", input["session_id"])
            return final_data

        # fix

        if type(result_timeline['recipients']) == list:
            recepient_id = db.contacts_users.find_one({"username": result_timeline['recipients'][0]})

        else:
            recepient_id = db.contacts_users.find_one({"username": result_timeline['recipients']})

        message = ' '.join(result_timeline['message'])

        if recepient_id == None:
            is_group = True
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "message": message, 'group': True,
                          "first_timestamp": first_timestamp}

            print("RECEPIENT_LIST", result_timeline['recipients'], result_timeline['intent'])
            # fix4
            recepient_id = db.groups.find_one({"name": result_timeline['recipients'][0]})
            users_data['recepient_id'] = recepient_id['id']
        else:
            is_group = False
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "message": message, 'group': False,
                          "first_timestamp": first_timestamp}
            # fix
            if type(result_timeline['recipients']) == list:
                print("in here->>>", result_timeline['recipients'])
                if len(result_timeline['recipients']) > 1:
                    users_data['recepient_id'] = result_timeline['recipient_id']
                    print("USERS_DATA", result_timeline['recipient_id'], users_data)
                    final_data = generate_response.generate_SEND_MESSAGE_responses(result_timeline['recipients'],
                                                                                   result_timeline['recipient_id'],
                                                                                   users_data,
                                                                                   "execute", input["session_id"])

                    time1 = [
                        {"session_id": input["session_id"], "timestamp": input["timestamp"],
                         "user_id": input['user_id'],
                         "message": input["utterances"][0]["utterance"],
                         "machine_action": "execute", "intent": result_timeline['intent'],
                         "recipients": result_timeline['recipients'], "recipient_id": result_timeline['recipient_id']}]
                    result = timeline.insert(time1)
                    if result_timeline['intent'] == "SEND_MESSAGE":
                        latest_id = db.state.find_one({"user_id": result_timeline['user_id']},
                                                      sort=[('id', pymongo.DESCENDING)])
                        if type(result_timeline['message']) == list:
                            message = ' '.join(result_timeline['message'])
                        else:
                            message = result_timeline['message']
                            print("clipping_job audio_id", final_data['data']['payload']['audio_id'])
                        clipping_job = [
                            {"session_id": input["session_id"], "timestamp": input["timestamp"],
                             "user_id": input['user_id'],
                             "_id": final_data['data']['payload']['audio_id'], "transcript": latest_id['transcript'],
                             "message_body": message
                             }]
                        print("CLIPPING_JOB4", clipping_job)
                        result = db.clipping_jobs.insert(clipping_job)

                    return final_data

                # FIX TO BE IDENTIFIED
                else:
                    users_data['recepient_id'] = result_timeline['recipient_id'][0]
                    recepient_id = db.contacts_users.find_one({"username": result_timeline['recipients'][0]})
                    time1 = [
                        {"session_id": input["session_id"], "timestamp": input["timestamp"],
                         "user_id": input['user_id'],
                         "message": input["utterances"][0]["utterance"],
                         "machine_action": "execute", "intent": result_timeline['intent'],
                         "recipients": result_timeline['recipients'], "recipient_id": result_timeline['recipient_id']}]
                    result = timeline.insert(time1)

                    if result_timeline['intent'] == "SEND_MESSAGE":

                        final_data = generate_response.generate_SEND_MESSAGE_responses(result_timeline['recipients'],
                                                                                       result_timeline['recipient_id'],
                                                                                       users_data,
                                                                                       "execute", input["session_id"])
                        latest_id = db.state.find_one({"user_id": result_timeline['user_id']},
                                                      sort=[('id', pymongo.DESCENDING)])
                        print("new_audio_id", final_data['data']['payload']['audio_id'])
                        if type(result_timeline['message']) == list:
                            message = ' '.join(result_timeline['message'])
                        else:
                            message = result_timeline['message']

            else:
                recepient_id = db.contacts_users.find_one({"username": result_timeline['recipients']})

                print("RECEPIENT_ID", recepient_id)
                users_data['recepient_id'] = recepient_id['uid']
                print("RECIP", recepient_id['uid'])
                time1 = [
                    {"session_id": input["session_id"], "timestamp": input["timestamp"],
                     "user_id": input['user_id'],
                     "message": input["utterances"][0]["utterance"],
                     "machine_action": "execute", "intent": result_timeline['intent'],
                     "recipients": result_timeline['recipients'], "recipient_id": result_timeline['recipient_id']}]
                result = timeline.insert(time1)
                latest_id = db.state.find_one({"user_id": result_timeline['user_id']},
                                              sort=[('_id', pymongo.DESCENDING)])
                if type(result_timeline['message']) == list:
                    message = ' '.join(result_timeline['message'])
                else:
                    message = result_timeline['message']
                clipping_job = [
                    {"session_id": input["session_id"], "timestamp": input["timestamp"],
                     "user_id": input['user_id'],
                     "_id": final_data['data']['payload']['audio_id'], "transcript": latest_id['transcript'],
                     "message": message
                     }]
                print("CLIPPING_JOB1", clipping_job)
                result = db.clipping_jobs.insert(clipping_job)
        add_state("execute", result_timeline['recipients'], '', '', time1)

        if result_timeline['intent'] == "MAKE_CALL":
            print(" in here2222")
            if is_group:
                final_data = generate_response.generate_MAKE_CALL_responses(recepient_id['name'],
                                                                            result_timeline['recipients'],
                                                                            users_data,
                                                                            "execute", input["session_id"])
            else:
                final_data = generate_response.generate_MAKE_CALL_responses(recepient_id['username'],
                                                                            result_timeline['recipients'],
                                                                            users_data,
                                                                            "execute", input["session_id"])

            return final_data
        else:
            latest_id = db.state.find_one({"user_id": result_timeline['user_id']}, sort=[('id', pymongo.DESCENDING)])
            if is_group:
                final_data = generate_response.generate_SEND_MESSAGE_responses(recepient_id['name'],
                                                                               result_timeline['recipients'],
                                                                               users_data,
                                                                               "execute", input["session_id"])
            else:

                final_data = generate_response.generate_SEND_MESSAGE_responses(recepient_id['username'],
                                                                               result_timeline['recipients'],
                                                                               users_data,
                                                                               "execute", input["session_id"])


                # final_data = generate_response.generate_SEND_MESSAGE_responses('', result_timeline['recipients'],
                # users_data,
                # "execute", input["session_id"])
            clipping_job = [
                {"session_id": input["session_id"], "timestamp": input["timestamp"],
                 "user_id": input['user_id'],
                 "_id": final_data['data']['payload']['audio_id'], "transcript": latest_id['transcript'],
                 "message_body": message
                 }]
            print("CLIPPING_JOB3", clipping_job)
            result = db.clipping_jobs.insert(clipping_job)

            return final_data

    elif result_timeline['machine_action'] == "other":
        users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                      "user_id": input["user_id"], "recepient_id": input['user_id'],
                      "message": input["utterances"][0]["utterance"]}
        time1 = [
            {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
             "message": input["utterances"][0]["utterance"],
             "machine_action": "other", "intent": result_timeline['intent'],
             "recipients": []}]
        result = timeline.insert(time1)
        add_state("other", [input['user_id']], '', '', time1)
        if result_timeline['intent'] == "MAKE_CALL":
            final_data = generate_response.generate_MAKE_CALL_responses(input1, [], users_data,
                                                                        "other", input["session_id"])
        else:
            final_data = generate_response.generate_CANCEL_MESSAGE_responses(input1, [], users_data,
                                                                             "other", input["session_id"])
            return final_data

    elif result_timeline['machine_action'] == "request":
        first_name, last_name, nick_name, full_name = Contact_matching.load_contact_names(
            input['user_id'])
        print("CONTACTS", first_name, last_name, full_name, nick_name)

        recepient_matched, is_group_matched = Contact_matching.match_contacts(input["utterances"][0]["utterance"],
                                                                              grapheme_to_phoneme(
                                                                                  [input["utterances"][0]["utterance"]],
                                                                                  False), input['user_id'])

        print("Recep", recepient_matched, is_group_matched)

        if recepient_matched == None and input1 not in first_name and input1 not in last_name and input1 not in full_name and input1 not in nick_name:

            recipient = []
            machine_action = "other"
            time1 = [
                {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
                 "message": input["utterances"][0]["utterance"],
                 "machine_action": "other", "intent": result_timeline['intent'],
                 "recipients": recipient}]
            result = timeline.insert(time1)
            users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"],
                          "user_id": input["user_id"], "recepient_id": input['user_id'],
                          "message": input["utterances"][0]["utterance"]}
            add_state("other", [recipient], '', '', time1)
            if result_timeline['intent'] == "MAKE_CALL":
                final_data = generate_response.generate_MAKE_CALL_responses([input1], [], users_data,
                                                                            "other", input["session_id"])
            else:
                final_data = generate_response.generate_SEND_MESSAGE_responses([input1], [], users_data,
                                                                               "other", input["session_id"])
                # final_data1 = generate_response.generate_CANCEL_MESSAGE_responses(input1, [], users_data,
                # "other", input["session_id"])

            return final_data




        elif input1 in first_name:
            recipient = full_name[first_name.index(input1)]

        elif input1 in last_name:
            recipient = full_name[last_name.index(input1)]
        elif input1 in nick_name:
            recipient = full_name[nick_name.index(input1)]
        elif input1 in full_name:
            recipient = full_name.index(input1)
        else:
            recipient = recepient_matched[0]
        recipient_id = db.contacts_users.find_one({"username": recipient})['uid']
        machine_action = "confirm"
        print("in here---newly added", recipient_id, input)
        time1 = [
            {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
             "message": result_timeline['message'],
             "machine_action": "confirm", "intent": result_timeline['intent'],
             "recipients": [recipient], "recipient_id": [recipient_id]}]
        result = timeline.insert(time1)
        add_state("confirm", [recipient], '', '', time1)
        users_data = {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input["user_id"],
                      "message": result_timeline['message'], "recepient_id": [recipient_id], 'group': False}
        if result_timeline['intent'] == "MAKE_CALL":
            final_data = generate_response.generate_MAKE_CALL_responses([recipient], [recipient], users_data,
                                                                        "confirm", input["session_id"])
        else:
            final_data = generate_response.generate_SEND_MESSAGE_responses([recipient], [recipient], users_data,
                                                                           "confirm", input["session_id"])

        return final_data


@app.route("/v1/notifications", methods=["POST"])
def notifications():
    possible_utterances = ["yes", "Yes", "Okay", "Sure", "sure", "read", "Read", "sure", "Sure"]
    input = request.json
    print("input of incoming_message", input)
    db.logs.insert(input)
    user_id = input['contact_id']
    if "type" in input.keys():
        opt_param = input['type']
    else:
        opt_param = None
    print(user_id, opt_param)
    user_name = db.contacts_users.find_one({"uid": user_id})
    print("opt", opt_param)

    if opt_param != "audio_note" and opt_param != "text":
        # or utterances in possible_utterances:
        print("here?")
        utterances = input['message_body']
        # utterances = input["utterances"][0]["utterance"]
        result_timeline = db.timeline.find_one({"user_id": input['user_id'], "timestamp": input["timestamp"]},
                                               sort=[('_id', pymongo.DESCENDING)])

        machine_action = "execute"

        time1 = [
            {"session_id": input["session_id"], "timestamp": input["timestamp"], "user_id": input['user_id'],
             "message": utterances,
             "machine_action": "execute", "intent": 'INCOMING_MESSAGE_READ', "audio_id": result_timeline["audio_id"],
             "recipients": [], "sender_id": user_id}]
        add_state("execute", [], '', '', time1)
        final_data = generate_response.generate_INCOMING_MESSAGE_READ_responses(user_name['username'], input, "execute",
                                                                                input["session_id"], utterances, '')


        # result_timeline = db.timeline.find_one({"user_id": input['user_id'], "timestamp": input["timestamp"]},
        # sort=[('_id', pymongo.DESCENDING)])

    else:
        session = random.randint(1000000000, 10000000000)
        message = input['message_body'].split("|")[0]
        audio_id = db.clipping_jobs.find_one(sort=[('_id', pymongo.DESCENDING)])['audio_id']
        # audio = input['message_body'].split("|")[1]
        print("message", message)
        # audio = input['message_body'].split("|")[1]

        machine_action = "confirm"
        time1 = [
            {"session_id": session, "timestamp": input["timestamp"], "user_id": input['user_id'],
             "message": message,
             "machine_action": "confirm", "intent": 'INCOMING_MESSAGE_READ', "audio_id": audio_id,
             "recipients": [], "sender_id": user_id, "sender_name": user_name['username']}]
        add_state("confirm", [input['user_id']], '', '', time1)
        final_data = generate_response.generate_INCOMING_MESSAGE_READ_responses(user_name['username'], input, "confirm",
                                                                                session, [], audio_id)

    result = timeline.insert(time1)

    print("final_data", final_data)
    final_data["success"] = True
    return flask.jsonify(final_data)


if __name__ == "__main__":
    app.run(debug=True)
    # app.run(host="0.0.0.0", port=2525)
