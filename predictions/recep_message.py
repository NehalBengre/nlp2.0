import numpy as np
import pickle
import nltk
from keras.models import model_from_json

def check_recepient(input1,model):

    x_word2idx_intent = pickle.load(open("../Real_data_models/x_word2idx_advanced_recepient.pickle", "rb"))

    def get_encoding(test):

        test = test.split()

        value = np.zeros((1, 44), dtype=np.int32)

        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 44:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx_intent:
                value[0][j] = x_word2idx_intent[token]
            else:
                value[0][j] = x_word2idx_intent['<UNK>']
        return value

    x_test_seq = get_encoding(input1)
    predicted = model.predict(x_test_seq)
    for i in range(len(x_test_seq)):

        pred = [np.argmax(y) for y in predicted[i]]

    message = []
    recepient = ''
    recepient_list=[]

    input1 = str(input1).split(" ")

    for i in range(len(pred)):
        if pred[i] == 1:
            recepient = input1[i]
            if "." in recepient:
                recepient = recepient.replace(".", "")
            recepient_list.append(recepient)

        if pred[i] == 2:
            if i >= len(input1):
                message.append(" ")
            else:

                message.append(input1[i])
        i = i + 1


    return recepient_list, message
