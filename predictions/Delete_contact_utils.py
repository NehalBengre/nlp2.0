import nltk

def is_delete(sentence):
    tokenized_sentence = nltk.word_tokenize(sentence.lower())

    if "delete" in tokenized_sentence:
        index = tokenized_sentence.index("delete")
        if "and" in sentence:
            index1=tokenized_sentence.index("and")
            return [tokenized_sentence[index + 1], tokenized_sentence[index1 + 1]]
    elif "remove" in tokenized_sentence:
        index = tokenized_sentence.index("remove")
        if "and" in sentence:
            index1 = tokenized_sentence.index("and")
            return [tokenized_sentence[index + 1], tokenized_sentence[index1 + 1]]
        else:
            return [tokenized_sentence[index + 1]]
    elif "don't send to" in sentence.lower():
        index = tokenized_sentence.index("to")
        if "and" in sentence:
            index1 = tokenized_sentence.index("and")
            return [tokenized_sentence[index + 1], tokenized_sentence[index1 + 1]]
        else:
            return [tokenized_sentence[index + 1]]
    elif "don't call" in sentence.lower():
        index = tokenized_sentence.index("call")
        if "and" in sentence:
            index1 = tokenized_sentence.index("and")
            return [tokenized_sentence[index + 1], tokenized_sentence[index1 + 1]]
        else:
            return [tokenized_sentence[index + 1]]