import pymongo as pymongo
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA

client = MongoClient()
client = MongoClient('localhost', 27017)
db = client['NLP2']

contact_groups = db.contact_groups

flatten_phone_features = []
with open('phone_features.txt', 'r') as f:
    for line in f:
        line = line.strip('\n').split(',')
        phone = line[0]
        for feature in line[1:]:
            flatten_phone_features.append([phone, feature.strip(), 1])

    phone_feature_df = (pd.DataFrame(flatten_phone_features,
                                     columns=['phone', 'feature', 'value'])
                        .pivot(index='phone', columns='feature', values='value')
                        .fillna(0))

    X = phone_feature_df.values

    y = phone_feature_df.index.tolist()

    explained_variance = []

    for i in range(2, 30):
        pca = PCA(n_components=i)
        pca.fit(X)

        explained_variance.append(pca.explained_variance_ratio_.sum())
pca = PCA(n_components=10)
pca.fit_transform(X)

from sklearn.metrics.pairwise import cosine_similarity

cos_sim = cosine_similarity(X, dense_output=True)


def load_contact_names(sender):
    full_name = []

    first_name = []
    last_name = []
    nick_name = []
    sender = db.contacts_users.find_one({"uid": sender})

    sender_name = sender['username']

    users = db.contact_groups.find({"list_users": {"$in": [sender_name]}})

    contact_list = []

    for values in users:

        for v in values['list_users']:
            if v != sender_name and v not in contact_list:
                contact_list.append(v)

    users = db.contacts_users.find({"username": {"$in": contact_list}})

    for values in users:

        name = values['username'].split(" ")
        full_name.append(values['username'])
        first_name.append(name[0])
        if len(name) > 1:
            last_name.append(name[1])
        nick_name.append(values['nickname'])
    return first_name, last_name, nick_name, full_name


def load_contacts(sender):
    sender = db.contacts_users.find_one({"uid": sender})
    sender_name = sender['username']

    users = db.teams.find({"list_users": {"$in": [sender['uid']]}})

    contact_list = []

    for values in users:

        for v in values['list_users']:
            if v not in contact_list:
                contact_list.append(v)

    users = db.contacts_users.find({"uid": {"$in": contact_list}})

    full_name_phoneme = []
    contact_list = []
    first_name_phoneme = []
    last_name_phoneme = []
    nick_name_phoneme = []

    for values in users:

        if values['uid'] not in contact_list:

            if len(values['names_phonemes']) > 3:
                contact_list.append(values['username'])
                first_name_phoneme.append(values['names_phonemes'][0])
                last_name_phoneme.append(values['names_phonemes'][1])
                full_name_phoneme.append(values['names_phonemes'][2])
                nick_name_phoneme.append(values['names_phonemes'][3])

            # FIX4
            elif len(values['names_phonemes']) == 1:
                contact_list.append(values['username'])
                first_name_phoneme.append(values['names_phonemes'][0])
                last_name_phoneme.append(values['names_phonemes'][0])
                full_name_phoneme.append(values['names_phonemes'][0])
                nick_name_phoneme.append(values['names_phonemes'][0])
    return first_name_phoneme, last_name_phoneme, full_name_phoneme, nick_name_phoneme, contact_list

def load_groups(sender):
    sender = db.contacts_users.find_one({"uid": sender})

    sender_name = sender['username']
    group = db.contact_groups.find({"list_users": {"$in": [sender_name]}})

    groups_list = []
    groups_phoneme = []
    for values in group:
        users = db.groups.find_one({"id": values['id']})

        groups_list.append(users['name'])
        groups_phoneme.append(users['phoneme'])

    return groups_phoneme, groups_list


def compare_phonemes(P1, P2, y):
    score = 0

    for p1, p2 in zip(P1, P2):
        score += cos_sim[y.index(p1)][y.index(p2)]

    return score


def find_best_sub_match(phoneme1, phoneme2, y):
    phoneme1 = phoneme1[0]
    l = len(phoneme1)
    L = len(phoneme2)

    highest_score = 0
    for i in range(l + L - 1):
        P1 = phoneme1[max(0, l - 1 - i):l + L - i - 1]
        P2 = phoneme2[max(0, i - l + 1):i + 1]

        highest_score = max(highest_score, compare_phonemes(P1, P2, y))

    return highest_score


def match_contacts(user_says, user_says_phoneme, sender):
    print("User_says",user_says, user_says_phoneme, sender)
    if user_says == '':
        return []

    flatten_phone_features = []
    with open('phone_features.txt', 'r') as f:
        for line in f:
            line = line.strip('\n').split(',')
            phone = line[0]
            for feature in line[1:]:
                flatten_phone_features.append([phone, feature.strip(), 1])

        phone_feature_df = (pd.DataFrame(flatten_phone_features,
                                         columns=['phone', 'feature', 'value'])
                            .pivot(index='phone', columns='feature', values='value')
                            .fillna(0))

        X = phone_feature_df.values

        y = phone_feature_df.index.tolist()

    first_name_phoneme, last_name_phoneme, full_name_phoneme, nick_name_phoneme, contact_list = load_contacts(
        sender)
    print("contact_list",contact_list)
    foo = []

    for matching_name in [full_name_phoneme, first_name_phoneme, last_name_phoneme, nick_name_phoneme]:

        u_score = []
        c_score = []
        for i in range(len(matching_name)):
            match_score = find_best_sub_match(user_says_phoneme, matching_name[i], y)

            match_score_norm_speech = np.round(match_score / len(user_says), 4)
            match_score_norm_contact = np.round(match_score / len(matching_name[i]), 4)

            # content score, user score
            u_score.append(match_score_norm_speech)
            c_score.append(match_score_norm_contact)

            i = i + 1

        top_matches_by_u_score = list(zip(contact_list,
                                          u_score))

        top_matches_by_c_score = list(zip(contact_list,
                                          c_score))

        top_matches_by_u_score.sort(key=lambda x: -x[1])
        top_matches_by_c_score.sort(key=lambda x: -x[1])

        # print("matching speech rate")
        foo1 = [x for x in top_matches_by_u_score[:10] if x[1] >= 0.8]
        for x in top_matches_by_u_score[:10]:
            if x[1] >= 0.1:
                foo1.append(x)

        # print("matching contact name rate")
        foo2 = [x for x in top_matches_by_c_score[:10] if x[1] >= 0.8]

        if foo1 and foo2:
            for x in foo1:
                if x[0] in list(zip(*foo2))[0]:
                    i = list(zip(*foo2))[0].index(x[0])
                    foo.append((x[0], x[1], foo2[i][1]))

        foo.sort(key=lambda x: -x[2])
        foo.sort(key=lambda x: -x[1])

        result = foo

        persons = []
        if len(result) == 1:
            persons.append(result[0][0])
        else:
            for i in range(1, len(result)):

                if result[i - 1][1] - result[i][1] < 0.1 or result[i - 1][2] - result[i][2] < 0.1:
                    persons.append(result[i - 1])
                i = i + 1
        final_people = []
        for values in persons:
            final_people.append(values[0])
        final_people = list(set(final_people))


        # GROUPS
        groups_phoneme, groups_list = load_groups(sender)
        print("Groups Phoneme",groups_phoneme)

        foo_1 = []
        u_score = []
        c_score = []
        for matching_name in groups_phoneme:
            match_score = find_best_sub_match(user_says_phoneme, matching_name, y)

            match_score_norm_speech = np.round(match_score / len(user_says), 4)
            match_score_norm_contact = np.round(match_score / len(matching_name), 4)

            # content score, user score
            u_score.append(match_score_norm_speech)
            c_score.append(match_score_norm_contact)

        top_matches_by_u_score = list(zip(groups_list,
                                          u_score))

        top_matches_by_c_score = list(zip(groups_list,
                                          c_score))

        top_matches_by_u_score.sort(key=lambda x: -x[1])
        top_matches_by_c_score.sort(key=lambda x: -x[1])

        # print("matching speech rate")
        foo1 = [x for x in top_matches_by_u_score[:10] if x[1] >= 0.7]
        for x in top_matches_by_u_score[:10]:
            if x[1] >= 0.1:
                foo1.append(x)

        # print("matching contact name rate")
        foo2 = [x for x in top_matches_by_c_score[:10] if x[1] >= 0.5]

        if foo1 and foo2:
            for x in foo1:
                if x[0] in list(zip(*foo2))[0]:
                    i = list(zip(*foo2))[0].index(x[0])
                    foo_1.append((x[0], x[1], foo2[i][1]))

        foo_1.sort(key=lambda x: -x[2])
        foo_1.sort(key=lambda x: -x[1])

        result2 = foo_1

    groups = []
    for values in result2:
        groups.append(values[0])

    final_groups = list(set(groups))

    if result == []:
        return final_groups, True
    if result2 == []:
        return final_people, False

    if result2[0][1] > result[0][1]:

        return final_groups, True
    else:

        return final_people, False


