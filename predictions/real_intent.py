import numpy as np
import pickle
import nltk
from keras.models import model_from_json
def check_intent(input1,model):

    x_word2idx_intent = pickle.load(open("../Real_data_models/x_word2idx_intent.pickle", "rb"))

    def get_encoding(test):

        test = test.split()

        value = np.zeros((1, 44), dtype=np.int32)

        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 44:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx_intent:
                value[0][j] = x_word2idx_intent[token]
            else:
                value[0][j] = x_word2idx_intent['<UNK>']
        return value

    x_test_seq = get_encoding(input1)

    predicted = model.predict(x_test_seq)
    if predicted[0] < 0.5:
        pred = 0
    else:
        pred = 1

    if pred == 0:
        return "SEND_MESSAGE"
    else:
        return "MAKE_CALL"

