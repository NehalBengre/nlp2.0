import numpy as np
import pickle
import nltk
from keras.models import model_from_json

import os

def check_intent(input1, model):
    loaded_model3 = model

    print(os.getcwd())
    x_word2idx_add_contacts = pickle.load(open("../Real_data_models/x_word2idx_advanced_recepient.pickle", "rb"))

    def get_encoding(test):

        test = test.split()

        value = np.zeros((1, 44), dtype=np.int32)

        for j, token in enumerate(test):
            # truncate long Titles

            if j >= 44:
                break
            token = token.lower()
            # represent each token with the corresponding index
            if token in x_word2idx_add_contacts:
                value[0][j] = x_word2idx_add_contacts[token]
            else:
                value[0][j] = x_word2idx_add_contacts['<UNK>']
        return value

    x_test_seq = get_encoding(input1)
    print(x_test_seq)


    predicted = loaded_model3.predict(x_test_seq)
    print(predicted[0])
    #for i in range(len(x_test_seq)):

    pred = list(predicted[0]).index(max(list(predicted[0])))
    intent = ''
    if pred == 0:
        intent = 'SEND_MESSAGE'
    elif pred == 1:
        intent = 'MAKE_CALL'
    elif pred == 2:
        intent = 'ADD_CONTACT'
    else:
        intent = 'DELETE_CONTACT'
    return intent


