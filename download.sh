yes | sudo apt-get install unzip

cd ..

echo "=====  Download CUDA drivers and gpu install script ====="
# TODO move these outside of personal Dropbox, they are ~300MB
curl -L https://www.dropbox.com/sh/79qvq055alup43c/AAAlhqsT63oJoev7kDvzXrMDa?dl=1 > gpu_install.zip
unzip gpu_install.zip

# Download DB file
wget https://www.smartear.ai/data/mongodump-2018-09-29.tar.gz
tar xvzf mongodump-2018-09-29.tar.gz

