

In order to install this on a new machine run the following scripts:


./download.sh  - only run once to download CUDA and Mongo dumps
./install.sh  - you can re-run this if you encounter issues.

Then follow the instructions by the end of install.sh

If you're able to start the server, make sure to also install and restore the MongoDB database:

./start_and_restore_mongodb.sh
