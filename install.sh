
sudo apt-get update
yes | sudo apt-get upgrade
yes | sudo apt-get install curl screen git unzip
yes | sudo apt-get install python3-pip python3-dev python3-venv

set -e

cd ..

echo "=====   Install basic linear algebra and build tools ====="
yes | sudo apt-get install build-essential cmake git unzip pkg-config libopenblas-dev liblapack-dev

echo "=====  Install scientific suite ====="
yes | sudo apt-get install python-numpy python-scipy python-matplotlib python-yaml

echo "=====  Install HDF5 ====="
yes | sudo apt-get install libhdf5-serial-dev python-h5py

echo "=====  Install graphviz and pydot for visualization ====="
yes | sudo apt-get install graphviz
sudo pip3 install pydot-ng

echo "=====  Install OpenCV ====="
yes | sudo apt-get install python-opencv

echo "=====  Installing CUDA ====="
wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb

sudo dpkg -i cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
echo "=====  Importing CUDA key ====="
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
set +e
yes | sudo apt-get update
set -e
echo "=====  Importing CUDA key ====="
yes | sudo apt-get install cuda-8-0

echo "=====  Install cuDNN drivers ====="
yes | sudo dpkg -i libcudnn7_7.3.1.20-1+cuda10.0_amd64.deb
yes | sudo dpkg -i libcudnn7-dev_7.3.1.20-1+cuda10.0_amd64.deb

# echo "=====  Download a special version of tensorflow ====="
# pip3 install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.11.0-cp36-cp36m-linux_x86_64.whl

yes | sudo pip3 install tensorflow-gpu==1.3
yes | sudo pip3 install g2p_en
yes | sudo pip3 install keras
echo "=====  Install Theano ====="
pip3 install theano

echo "=====  Set up a virtualenvironment which uses system-wide dependencies ====="
python3 -m venv --system-site-packages nlpenv
source nlpenv/bin/activate

cd nlp2.0/
pip3 install flask pymongo sklearn pandas nltk keras pylint pytest

# For some reason g2p_en needs to be reinstalled
sudo pip3 uninstall g2p_en
sudo pip3 install g2p_en

# pip3 install -r requirements.txt

echo "There are TWO MORE STEPS remaining which you need to do manually:"
echo "  1) edit $HOME/.keras/keras.json to use theano instead of tensorflow"
echo "  2) Manually download 'punkt' from nltk. "
echo "      >>> import nltk "
echo "      >>> nltk.download('punkt') "
echo ""
echo ""
echo "After all of this is done you can start the DEVELOPMENT server by running "
echo "> cd predictionsy "
echo "> source ../../nlpenv/bin/activate "
echo "> python3 Predict_ContactSync.py "
echo "from the predictions folder. If you encounter any issues when importing g2p_en you can  "
echo "delete the existing tensorflow installation and reinstall g2p_en again"
echo "> sudo rm -rf /usr/local/lib/python3.5/dist-packages/tensorflow "
echo "> sudo pip3 uninstall g2p_en "
echo "> sudo pip3 install g2p_en "

